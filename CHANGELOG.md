**Version v11 (master)**

- Update to eqasim 1.0.5
- Use modular mode choice (Update eqasim v1.0.1)
- Fix eqasim version
- Only use crowfly distance in location assignment
- Make compatible with eqasim
- Make pipeline deterministic (and request random seed from config)
- Add weighted version of ENTD
- Match activity chains by departement + person analysis per departement
- Add consumption units to households
- Add clean EGT output as CSV
- Added fix for innerParis attribute
- Update opportunities: Communes without work/education opportunity get a single one at the centroid
- Add departement flow analysis

**Version v10**

- Add innerParis attribute
- Add car_passenger to network
- Add home residence zone information
- BUGFIX: Income class for matching was shifted by 1
- Update pt2matsim to fixed version
- Add aire urbaine

**Version v9**

- Assign formerly unassigned children to households
- Update pt2matsim to 10.12
- Add analysis per person, per household and for income

**Version v8**

- Use discrete locations (from BPE) as primary facilities
- Write out home facilities
- Avoid negative departure times
