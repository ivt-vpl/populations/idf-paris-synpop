import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
import pandas as pd

def configure(context, require):
    require.stage("data.entd.cleaned")
    require.stage("data.egt.cleaned")
    require.stage("population.trips")

def make_chains(df, df_persons = None):
    df = df.sort_values(["person_id", "trip_id"])

    df.loc[df["purpose"] == "home", "purpose_simple"] = "H"
    df.loc[df["purpose"] == "work", "purpose_simple"] = "W"
    df.loc[df["purpose"] == "education", "purpose_simple"] = "E"
    df.loc[df["purpose"] == "shop", "purpose_simple"] = "S"
    df.loc[df["purpose"] == "leisure", "purpose_simple"] = "L"
    df.loc[df["purpose"] == "other", "purpose_simple"] = "X"

    df = df[["person_id", "purpose_simple"]].groupby("person_id").aggregate({
        "purpose_simple" : lambda x: "H" + "".join(x.values)
    }).reset_index()[["person_id", "purpose_simple"]]
    df.columns = ["person_id", "chain"]

    if df_persons is not None:
        df = pd.merge(df, df_persons[["person_id", "weight"]])
    else:
        df.loc[:, "weight"] = 1.0

    df.columns = ["person_id", "chain", "count"]
    df = df[["chain", "count"]].groupby("chain").sum().reset_index()
    df["weight"] = df["count"] / np.sum(df["count"])

    return df

def execute(context):
    # Extract all chain information
    df_egt_persons, df_egt_trips = context.stage("data.egt.cleaned")
    df_egt_chains = make_chains(df_egt_trips, df_egt_persons)

    df_entd_persons, df_entd_trips = context.stage("data.entd.cleaned")
    df_entd_trips = df_entd_trips[df_entd_trips["weekday"]]
    df_entd_chains = make_chains(df_entd_trips, df_entd_persons)

    df_synthetic_trips = context.stage("population.trips")
    df_synthetic_trips["purpose"] = df_synthetic_trips["following_purpose"]
    df_synthetic_chains = make_chains(df_synthetic_trips)

    # Sort them
    df_egt_chains = df_egt_chains.sort_values(by = "weight", ascending = False)
    df_entd_chains = df_entd_chains.sort_values(by = "weight", ascending = False)
    df_synthetic_chains = df_synthetic_chains.sort_values(by = "weight", ascending = False)

    plt.figure(figsize = (11, 4), dpi = 120)
    plt.subplot(1,3,1)
    plt.barh(np.arange(10)[::-1], width = df_entd_chains["weight"].values[:10], color = "C0")
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(10)[::-1]))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(df_entd_chains["chain"].values[:10]))
    plt.grid()
    plt.title("ENTD")
    plt.subplot(1,3,2)
    plt.barh(np.arange(10)[::-1], width = df_egt_chains["weight"].values[:10], color = "C1")
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(10)[::-1]))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(df_egt_chains["chain"].values[:10]))
    plt.grid()
    plt.title("EGT")
    plt.subplot(1,3,3)
    plt.barh(np.arange(10)[::-1], width = df_synthetic_chains["weight"].values[:10], color = "C2")
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(10)[::-1]))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(df_synthetic_chains["chain"].values[:10]))
    plt.grid()
    plt.title("Synthetic")
    plt.tight_layout()
    plt.savefig("%s/activity_chains.pdf" % context.cache_path)

    return "%s/activity_chains.pdf" % context.cache_path
