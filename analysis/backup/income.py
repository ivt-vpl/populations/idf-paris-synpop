import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
import pandas as pd

def configure(context, require):
    require.stage("data.spatial.zones")
    require.stage("population.income")
    require.stage("population.spatial.by_person.primary_zones")
    require.stage("population.sociodemographics")

def execute(context):
    df_communes = context.stage("data.spatial.zones")

    df_income = context.stage("population.income")
    df_spatial = context.stage("population.spatial.by_person.primary_zones")[0]
    df_persons = context.stage("population.sociodemographics")[["person_id", "household_id"]]
    df_persons = pd.merge(df_persons, df_income)
    df_persons = pd.merge(df_persons, df_spatial)
    df_households = df_persons.drop_duplicates("household_id")[["household_income", "zone_id"]]
    df_households["household_income"] *= 12
    df_households = pd.merge(df_households, df_communes)
    df_income = df_households.groupby("commune_id").median().reset_index()[["commune_id", "household_income"]]
    df_count = df_households.groupby("commune_id").size().reset_index(name = "obs")[["commune_id", "obs"]]
    df_income = pd.merge(df_income, df_count, on = ["commune_id"])[["commune_id", "household_income", "obs"]]
    df_income.columns = ["commune_id", "matsim_income", "obs"]

    df_filo = pd.read_excel(
        "%s/filo/FILO_DISP_COM.xls" % context.config["raw_data_path"],
        sheet_name = "ENSEMBLE", skiprows = 5
    )[["CODGEO", "Q215"]]
    df_filo.columns = ["commune_id", "reference_income"]

    minimum_income_observations = 1e3 * context.config["input_downsampling"]

    df_income = pd.merge(df_income, df_filo)
    df_income = df_income[df_income["obs"] > minimum_income_observations]
    maximum_income = 50000

    plt.figure(figsize = (5, 4), dpi = 120)
    plt.plot([0, maximum_income], [0, maximum_income], 'k--')
    plt.xlim([0, maximum_income])
    plt.ylim([0, maximum_income])
    plt.plot(df_income.loc[:, "reference_income"], df_income.loc[:, "matsim_income"], marker = '.', linestyle = "none")
    plt.xlabel("FILOSOFI")
    plt.ylabel("MATSim")
    plt.grid()
    plt.tight_layout()
    plt.title("Median yearly income by commune")
    plt.savefig("%s/income_by_municipality.pdf" % context.cache_path)
    plt.show()

    return "%s/income_by_municipality.pdf" % context.cache_path
