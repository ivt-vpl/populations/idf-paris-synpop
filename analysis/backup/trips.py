import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import numpy as np
import pandas as pd

#SOURCE = "population.trips"
#SOURCE = "matsim.analysis.population_trips"
SOURCE = "matsim.analysis.events_trips"

def configure(context, require):
    require.stage("data.entd.cleaned")
    require.stage("data.egt.cleaned")
    require.stage(SOURCE)

def extract_data(df_trips, df_persons = None):
    if df_persons is None:
        df_trips.loc[:, "weight"] = 1.0
    else:
        df_trips = pd.merge(df_trips, df_persons[["person_id", "weight"]])

    if not "crowfly_distance" in df_trips:
        df_trips.loc[:, "crowfly_distance"] = 1.0

    if not "routed_distance" in df_trips:
        df_trips.loc[:, "routed_distance"] = 1.0

    if "network_distance" in df_trips:
        df_trips["routed_distance"] = df_trips["network_distance"]

    if not "travel_time" in df_trips:
        df_trips["travel_time"] = df_trips["arrival_time"] - df_trips["departure_time"]

    df_trips = df_trips[["person_id", "weight", "mode", "routed_distance", "crowfly_distance", "travel_time"]]

    return pd.DataFrame(df_trips)

MODES = ["car", "pt", "bike", "walk", "car_passenger"]

def execute(context):
    # Extract all chain information
    df_egt_persons, df_egt_trips = context.stage("data.egt.cleaned")
    df_egt = extract_data(df_egt_trips, df_egt_persons)

    df_entd_persons, df_entd_trips = context.stage("data.entd.cleaned")
    df_entd_trips = df_entd_trips[df_entd_trips["weekday"]]
    df_entd = extract_data(df_entd_trips, df_entd_persons)

    df_synthetic_trips = context.stage(SOURCE)
    df_synthetic_trips["network_distance"] *= 1e3
    df_synthetic_trips["crowfly_distance"] *= 1e3
    df_synthetic = extract_data(df_synthetic_trips)

    analyse(context.cache_path, df_synthetic, df_egt, df_entd, False)

def analyse(output_path, df_synthetic, df_egt, df_entd, show_entd = True):
    # Total mode share (by trips)
    mode_share_egt = [np.sum(df_egt.loc[df_egt["mode"] == m, "weight"]) for m in MODES]
    mode_share_entd = [np.sum(df_entd.loc[df_entd["mode"] == m, "weight"]) for m in MODES]
    mode_share_synthetic = [np.sum(df_synthetic.loc[df_synthetic["mode"] == m, "weight"]) for m in MODES]

    mode_share_egt = np.array(mode_share_egt) / np.sum(mode_share_egt)
    mode_share_entd = np.array(mode_share_entd) / np.sum(mode_share_entd)
    mode_share_synthetic = np.array(mode_share_synthetic) / np.sum(mode_share_synthetic)

    plt.figure(figsize = (5, 4), dpi = 300)
    if show_entd: plt.barh(np.arange(len(MODES)), width = mode_share_entd, label = "ENTD", height = 0.25)
    plt.barh(np.arange(len(MODES)) + 0.25, width = mode_share_egt, label = "EGT", height = 0.25)
    plt.barh(np.arange(len(MODES)) + 0.5, width = mode_share_synthetic, label = "Synthetic", height = 0.25)
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(len(MODES)) + 0.25))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(MODES))
    plt.gca().xaxis.set_major_locator(ticker.FixedLocator(np.arange(100) * 0.1))
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: "%d%%" % (x * 100.0,)))
    plt.grid(), plt.legend(), plt.xlabel("Number of trips")
    plt.tight_layout()

    plt.savefig("%s/aggregate_mode_share_by_trips.png" % output_path)

    # Total mode share (by routed distance)
    if False:
        mode_share_egt = [np.sum(df_egt.loc[df_egt["mode"] == m, "weight"] * df_egt.loc[df_egt["mode"] == m, "routed_distance"]) for m in MODES]
        mode_share_entd = [np.sum(df_entd.loc[df_entd["mode"] == m, "weight"] * df_entd.loc[df_entd["mode"] == m, "routed_distance"]) for m in MODES]
        mode_share_synthetic = [np.sum(df_synthetic.loc[df_synthetic["mode"] == m, "weight"] * df_synthetic.loc[df_synthetic["mode"] == m, "routed_distance"]) for m in MODES]

        mode_share_egt = np.array(mode_share_egt) / np.sum(mode_share_egt)
        mode_share_entd = np.array(mode_share_entd) / np.sum(mode_share_entd)
        mode_share_synthetic = np.array(mode_share_synthetic) / np.sum(mode_share_synthetic)

        plt.figure(figsize = (5, 4), dpi = 300)
        if show_entd: plt.barh(np.arange(len(MODES)), width = mode_share_entd, label = "ENTD", height = 0.25)
        plt.barh(np.arange(len(MODES)) + 0.25, width = mode_share_egt, label = "EGT", height = 0.25)
        plt.barh(np.arange(len(MODES)) + 0.5, width = mode_share_synthetic, label = "Synthetic", height = 0.25)
        plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(len(MODES)) + 0.25))
        plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(MODES))
        plt.gca().xaxis.set_major_locator(ticker.FixedLocator(np.arange(100) * 0.1))
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: "%d%%" % (x * 100.0,)))
        plt.grid(), plt.legend(), plt.xlabel("Routed travel distance")
        plt.tight_layout()
        plt.savefig("%s/aggregate_mode_share_by_routed_distance.png" % output_path)

    # Total mode share (by crowfly distance)
    mode_share_egt = [np.sum(df_egt.loc[df_egt["mode"] == m, "weight"] * df_egt.loc[df_egt["mode"] == m, "crowfly_distance"]) for m in MODES]
    mode_share_synthetic = [np.sum(df_synthetic.loc[df_synthetic["mode"] == m, "weight"] * df_synthetic.loc[df_synthetic["mode"] == m, "crowfly_distance"]) for m in MODES]

    mode_share_egt = np.array(mode_share_egt) / np.sum(mode_share_egt)
    mode_share_synthetic = np.array(mode_share_synthetic) / np.sum(mode_share_synthetic)

    plt.figure(figsize = (5, 4), dpi = 300)
    plt.barh(np.arange(len(MODES)) + 0.25, width = mode_share_egt, label = "EGT", height = 0.25)
    plt.barh(np.arange(len(MODES)) + 0.5, width = mode_share_synthetic, label = "Synthetic", height = 0.25)
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(len(MODES)) + 0.25))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(MODES))
    plt.gca().xaxis.set_major_locator(ticker.FixedLocator(np.arange(100) * 0.1))
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: "%d%%" % (x * 100.0,)))
    plt.grid(), plt.legend(), plt.xlabel("Crowfly distance")
    plt.tight_layout()
    plt.savefig("%s/aggregate_mode_share_by_crowfly_distance.png" % output_path)

    # Total mode share (by travel time)
    mode_share_egt = [np.sum(df_egt.loc[df_egt["mode"] == m, "weight"] * df_egt.loc[df_egt["mode"] == m, "travel_time"]) for m in MODES]
    mode_share_entd = [np.sum(df_entd.loc[df_entd["mode"] == m, "weight"] * df_entd.loc[df_entd["mode"] == m, "travel_time"]) for m in MODES]
    mode_share_synthetic = [np.sum(df_synthetic.loc[df_synthetic["mode"] == m, "weight"] * df_synthetic.loc[df_synthetic["mode"] == m, "travel_time"]) for m in MODES]

    mode_share_egt = np.array(mode_share_egt) / np.sum(mode_share_egt)
    mode_share_entd = np.array(mode_share_entd) / np.sum(mode_share_entd)
    mode_share_synthetic = np.array(mode_share_synthetic) / np.sum(mode_share_synthetic)

    plt.figure(figsize = (5, 4), dpi = 300)
    if show_entd: plt.barh(np.arange(len(MODES)), width = mode_share_entd, label = "ENTD", height = 0.25)
    plt.barh(np.arange(len(MODES)) + 0.25, width = mode_share_egt, label = "EGT", height = 0.25)
    plt.barh(np.arange(len(MODES)) + 0.5, width = mode_share_synthetic, label = "Synthetic", height = 0.25)
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(len(MODES)) + 0.25))
    plt.gca().yaxis.set_major_formatter(ticker.FixedFormatter(MODES))
    plt.gca().xaxis.set_major_locator(ticker.FixedLocator(np.arange(100) * 0.1))
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: "%d%%" % (x * 100.0,)))
    plt.grid(), plt.legend(), plt.xlabel("Travel time")
    plt.tight_layout()
    plt.savefig("%s/aggregate_mode_share_by_travel_time.png" % output_path)

    # Mode share by crowfly distance class
    number_of_distance_bins = 15
    distance_quantiles = (np.arange(number_of_distance_bins) + 1) / (number_of_distance_bins + 1)
    distance_boundaries = np.array([np.percentile(df_egt["crowfly_distance"].values, 100.0 * q) for q in distance_quantiles])

    df_egt.loc[:, "distance_class"] = np.digitize(df_egt["crowfly_distance"], distance_boundaries, right = True)
    df_synthetic.loc[:, "distance_class"] = np.digitize(df_synthetic["crowfly_distance"], distance_boundaries, right = True)

    plt.figure(figsize = (5, 4), dpi = 300)

    egt_values = np.zeros((len(MODES), len(distance_boundaries)))
    synthetic_values = np.zeros((len(MODES), len(distance_boundaries)))

    for mode_index, mode in enumerate(MODES):
        for c in range(len(distance_boundaries)):
            egt_values[mode_index,c] = np.sum(df_egt.loc[(df_egt["mode"] == mode) & (df_egt["distance_class"] == c), "weight"])
            synthetic_values[mode_index,c] = np.sum(df_synthetic.loc[(df_synthetic["mode"] == mode) & (df_synthetic["distance_class"] == c), "weight"])

    egt_values /= np.sum(egt_values, 0)[np.newaxis, :]
    synthetic_values /= np.sum(synthetic_values, 0)[np.newaxis, :]

    for mode_index, mode in enumerate(MODES):
        plt.plot(distance_boundaries * 1e-3, egt_values[mode_index, :], color = "C%d" % (mode_index + 0,), linestyle = ":", linewidth = 1.0)
        plt.plot(distance_boundaries * 1e-3, synthetic_values[mode_index, :], color = "C%d" % (mode_index + 0,), linestyle = "-", label = mode, linewidth = 1.0)

    for b in distance_boundaries:
        plt.plot([b * 1e-3, b * 1e-3], [0.0, 1.0], 'k', alpha = 0.25, linewidth = 0.5)

    for q in np.linspace(0, 1, 10):
        plt.plot([distance_boundaries[0] * 1e-3, distance_boundaries[-1] * 1e-3], [q, q], 'k', alpha = 0.25, linewidth = 0.5)

    plt.ylim([0.0, 1.0]), plt.xlim([distance_boundaries[0] * 1e-3, distance_boundaries[-1] * 1e-3])
    plt.gca().yaxis.set_major_locator(ticker.FixedLocator(np.arange(100) * 0.1))
    plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: "%d%%" % (x * 100.0,)))
    plt.xlabel("Crowfly distance [km]")
    plt.legend()
    plt.tight_layout()
    plt.savefig("%s/mode_share_by_crowfly_distance_class.png" % output_path)

    # Distributions by mode
    for mode in MODES:
        # Crowfly distance
        plt.figure(figsize = (5, 4), dpi = 300)
        maximum_distance = 20.0 * 1000.0

        values = df_egt[(df_egt["mode"] == mode) & (df_egt["crowfly_distance"] < maximum_distance)].sort_values(by = "crowfly_distance")[["crowfly_distance", "weight"]].values
        plt.plot(values[:,0] * 1e-3, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C1", label = "EGT")
        values = df_synthetic[(df_synthetic["mode"] == mode) & (df_synthetic["crowfly_distance"] < maximum_distance)].sort_values(by = "crowfly_distance")[["crowfly_distance", "weight"]].values
        plt.plot(values[:,0] * 1e-3, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C2", label = "Synthetic")

        plt.xlabel("Crowfly distance [km]")
        plt.ylabel("CDF")
        plt.grid(), plt.legend()
        plt.title("Crowfly distance distribution: %s" % (mode,))
        plt.tight_layout()
        plt.savefig("%s/crowfly_distance_distribution_%s.png" % (output_path, mode))

        # Routed distance
        if False:
            plt.figure(figsize = (5, 4), dpi = 300)

            values = df_entd[(df_entd["mode"] == mode) & (df_entd["routed_distance"] < maximum_distance)].sort_values(by = "routed_distance")[["routed_distance", "weight"]].values
            if show_entd: plt.plot(values[:,0] * 1e-3, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C0", label = "ENTD")
            values = df_egt[(df_egt["mode"] == mode) & (df_egt["routed_distance"] < maximum_distance)].sort_values(by = "routed_distance")[["routed_distance", "weight"]].values
            plt.plot(values[:,0] * 1e-3, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C1", label = "EGT")
            values = df_synthetic[(df_synthetic["mode"] == mode) & (df_synthetic["routed_distance"] < maximum_distance)].sort_values(by = "routed_distance")[["routed_distance", "weight"]].values
            plt.plot(values[:,0] * 1e-3, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C2", label = "Synthetic")

            plt.xlabel("Routed travel distance [km]")
            plt.ylabel("CDF")
            plt.grid(), plt.legend()
            plt.title("Routed distance distribution: %s" % (mode,))
            plt.tight_layout()
            plt.savefig("%s/routed_distance_distribution_%s.png" % (output_path, mode))

        # Travel time
        plt.figure(figsize = (5, 4), dpi = 300)
        maximum_travel_time = 4 * 3600.0

        values = df_entd[(df_entd["mode"] == mode) & (df_entd["travel_time"] < maximum_travel_time)].sort_values(by = "travel_time")[["travel_time", "weight"]].values
        if show_entd: plt.plot(values[:,0] / 60, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C0", label = "ENTD")
        values = df_egt[(df_egt["mode"] == mode) & (df_egt["travel_time"] < maximum_travel_time)].sort_values(by = "travel_time")[["travel_time", "weight"]].values
        plt.plot(values[:,0] / 60, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C1", label = "EGT")
        values = df_synthetic[(df_synthetic["mode"] == mode) & (df_synthetic["travel_time"] < maximum_travel_time)].sort_values(by = "travel_time")[["travel_time", "weight"]].values
        plt.plot(values[:,0] / 60, np.cumsum(values[:,1]) / np.sum(values[:,1]), color = "C2", label = "Synthetic")

        plt.xlabel("Travel time [min]")
        plt.ylabel("CDF")
        plt.grid(), plt.legend()
        plt.title("Travel time distribution (%s)" % (mode,))
        plt.tight_layout()
        plt.savefig("%s/travel_time_distribution_%s.png" % (output_path, mode))

if __name__ == "__main__":
    import sys, pickle
    matplotlib.rcParams['axes.prop_cycle'] = matplotlib.cycler(color = ["c", "m", "y", "k", "g"])

    synthetic_path = sys.argv[1]
    egt_path = sys.argv[2]
    output_path = sys.argv[3]

    with open(egt_path, "rb") as f:
        df_egt_persons, df_egt_trips = pickle.load(f)

    df_egt = extract_data(df_egt_trips, df_egt_persons)

    df_synthetic_trips = pd.read_csv(synthetic_path, sep = ";")
    df_synthetic_trips["network_distance"] *= 1e3
    df_synthetic_trips["crowfly_distance"] *= 1e3
    df_synthetic = extract_data(df_synthetic_trips)

    analyse(output_path, df_synthetic, df_egt, df_egt, False)
