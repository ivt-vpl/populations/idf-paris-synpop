import gzip
import numpy as np
import pandas as pd
import pandas.io.sql as pd_sql
import sqlite3
import matplotlib.pyplot as plt
import matplotlib.ticker as tck

def configure(context, require):
    require.stage("population.spatial.by_person.primary_zones")
    require.stage("data.spatial.zones")
    require.stage("population.sociodemographics")
    require.stage("data.od.cleaned")
    require.stage("data.egt.cleaned")

def execute(context):
    # Find the amount of activity chains with a work activity compared to the number of employed people per departement
    df_egt_persons, df_egt_trips = context.stage("data.egt.cleaned")

    df_egt_trips = df_egt_trips[df_egt_trips["purpose"] == "work"].drop_duplicates("person_id")[["person_id"]]
    df_egt_persons = pd.merge(df_egt_persons, df_egt_trips, how = "left")
    df_egt_persons = df_egt_persons[["weight", "employed", "departement_id", "has_work_trip"]]

    df_partial = df_egt_persons[df_egt_persons["employed"]]
    df_partial = df_partial[["departement_id", "has_work_trip", "weight"]].groupby(["departement_id", "has_work_trip"]).sum().reset_index()

    df_total = df_partial[["departement_id", "weight"]].groupby("departement_id").sum().rename({"weight" : "total"}, axis = 1)
    df_partial = pd.merge(df_partial, df_total, on = "departement_id")
    df_partial = df_partial[df_partial["has_work_trip"]]
    df_partial["factor"] = df_partial["weight"] / df_partial["total"]
    df_partial = df_partial[["departement_id", "factor"]]

    # Load primary zones of all agents
    df_home, df_work, _ = context.stage("population.spatial.by_person.primary_zones")
    df_zones = context.stage("data.spatial.zones")[["zone_id", "commune_id"]]
    df_persons = context.stage("population.sociodemographics")

    df_persons = df_persons[
        (df_persons["age"] >= 15) & df_persons["employed"]
    ][["person_id", "household_id"]]

    df_home = pd.merge(df_home, df_zones, on = "zone_id")
    df_home = pd.merge(df_persons, df_home, on = "household_id")

    df_work = pd.merge(df_work, df_zones, on = "zone_id")
    df_work = pd.merge(df_persons, df_work, on = "person_id")

    df_home = df_home[["person_id", "commune_id"]].rename({"commune_id" : "origin_id"}, axis = 1)
    df_work = df_work[["person_id", "commune_id"]].rename({"commune_id" : "destination_id"}, axis = 1)

    df_flow = pd.merge(df_home, df_work, on = "person_id")
    df_flow = df_flow.groupby(["origin_id", "destination_id"]).size().reset_index(name = "synthetic_count")

    # Load reference OD data
    df_reference = context.stage("data.od.cleaned")[0].rename({"weight" : "reference_count"}, axis = 1)
    df_reference = df_reference.groupby(["origin_id", "destination_id"]).sum().reset_index()

    # Merge synthetic data set and reference for comparison
    df_comparison = pd.merge(df_flow, df_reference, on = ["origin_id", "destination_id"])
    df_comparison["synthetic_count"] /= context.config["input_downsampling"]

    df_comparison["origin_id"] = df_comparison["origin_id"] // 1000
    df_comparison["destination_id"] = df_comparison["destination_id"] // 1000

    df_comparison = df_comparison.groupby(["origin_id", "destination_id"]).sum().reset_index()

    # Apply correction factor from above
    df_comparison = pd.merge(df_comparison, df_partial.rename({"departement_id" : "origin_id"}, axis = 1))
    df_comparison["corrected_reference_count"] = df_comparison["reference_count"] * df_comparison["factor"]

    return df_comparison[[
        "origin_id", "destination_id", "reference_count", "corrected_reference_count", "synthetic_count"
    ]]
