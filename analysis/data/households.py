import numpy as np
import analysis.constants as c
from collections import OrderedDict
import data.constants as cd

def configure(context, require):
    require.stage("data.hts")
    require.stage("data.census.cleaned")
    require.stage("population.sociodemographics")

def extract_analysis_data(df):
    analysis = OrderedDict()

    # Total count
    analysis["total"] = np.sum(df["weight"])

    # Number of vehicles
    df["number_of_vehicles_class"] = np.digitize(df["number_of_vehicles"], c.NUMBER_OF_VEHICLES_BOUNDARIES, right = True)

    for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT):
        analysis["number_of_vehicles_class_%d" % k] = np.sum(df.loc[df["number_of_vehicles_class"] == k, "weight"])

    # Number of bikes
    if "number_of_bikes" in df.columns:
        df["number_of_bikes_class"] = np.digitize(df["number_of_bikes"], c.NUMBER_OF_VEHICLES_BOUNDARIES, right = True)

        for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT):
            analysis["number_of_bikes_class_%d" % k] = np.sum(df.loc[df["number_of_bikes_class"] == k, "weight"])

    # Houeshold size
    df["household_size_class"] = np.digitize(df["household_size"], c.HOUSEHOLD_SIZE_CLASS_BOUNDARIES, right = True)

    for k in range(c.HOUSEHOLD_SIZE_CLASS_COUNT):
        analysis["household_size_class_%d" % k] = np.sum(df.loc[df["household_size_class"] == k, "weight"])

    return analysis

def execute(context):
    df_hts = context.stage("data.hts")[0]
    df_census = context.stage("data.census.cleaned")
    df_synthetic = context.stage("population.sociodemographics")

    df_hts = df_hts.sort_values(by = ["household_id", "person_id"]).drop_duplicates("household_id", keep = "first")
    df_census = df_census.sort_values(by = ["household_id", "person_id"]).drop_duplicates("household_id", keep = "first")
    df_synthetic = df_synthetic.sort_values(by = ["household_id", "person_id"]).drop_duplicates("household_id", keep = "first")

    factor = context.config["input_downsampling"] if "input_downsampling" in context.config else 1.0
    df_synthetic["weight"] = 1.0 / factor

    return {
        "hts" : extract_analysis_data(df_hts),
        "census" : extract_analysis_data(df_census),
        "synthetic" : extract_analysis_data(df_synthetic)
    }
