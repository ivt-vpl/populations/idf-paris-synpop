import numpy as np
import analysis.constants as c
from collections import OrderedDict
import data.constants as cd
import pandas as pd

def configure(context, require):
    require.stage("data.egt.cleaned")
    require.stage("population.income")
    require.stage("data.filo")
    require.stage("data.filo_region")
    require.stage("population.upscaled")

def execute(context):
    df_synthetic = context.stage("population.income")
    df_egt = context.stage("data.egt.cleaned")[0]

    # Start: Calculations for unité de consommation

    df_egt = context.stage("data.egt.cleaned")[0]
    df_egt["under_14"] = df_egt["age"] < 14

    df_count = df_egt[["household_id"]].groupby("household_id").size().reset_index(name = "person_count")
    df_egt = pd.merge(df_egt.drop_duplicates("household_id"), df_count)

    df_egt["under_14"] += df_egt["household_size"] - df_egt["person_count"]
    df_egt["over_14"] = df_egt["household_size"] - df_egt["under_14"]

    df_egt["unites"] = df_egt["under_14"] * 0.3 + 1.0 + np.maximum(0, df_egt["over_14"] - 1) * 0.5

    # End: Calculations for unité de consommation

    # Start: Impute EGT household income
    egt_class_values = np.array([400, 1000, 1400, 1800, 2200, 2700, 3250, 4000, 5000, 7000]) * 12

    df_egt = df_egt.dropna()
    df_egt["household_income"] = egt_class_values[df_egt["household_income_class"].values.astype(np.int)]
    df_egt["household_income"] /= df_egt["unites"]
    # End: Impute EGT household income

    analysis = {"overall_income_distributions" : {}}

    # A) Overall income distributions
    # A1) Synthetic
    synthetic_quantiles = np.arange(1,20) / 20
    synthetic_values = np.array([np.percentile(df_synthetic["household_income"].values * 12, q * 100.0) for q in synthetic_quantiles])
    analysis["overall_income_distributions"]["synthetic"] = (synthetic_quantiles, synthetic_values)

    # A2) FILOSOFI
    filo_quantiles = np.arange(1,10) / 10
    filo_values = context.stage("data.filo_region")
    analysis["overall_income_distributions"]["filo"] = (filo_quantiles, filo_values)

    # A3) EGT
    df_egt = df_egt.sort_values(by = "household_income")
    df_egt_cdf = df_egt[["household_weight", "household_income"]].groupby("household_income").sum().reset_index()

    egt_quantiles = df_egt_cdf["household_weight"].values
    egt_quantiles = np.cumsum(egt_quantiles)
    egt_quantiles /= egt_quantiles[-1]

    egt_values = df_egt_cdf["household_income"].values
    analysis["overall_income_distributions"]["egt"] = (egt_quantiles, egt_values)

    # B) Median per commune
    df_filo_medians = context.stage("data.filo")["total"][["commune_id", "q5"]]
    df_filo_medians.columns = ["commune_id", "filo_median"]

    df_population = context.stage("population.upscaled")[["household_id", "commune_id"]].drop_duplicates("household_id")
    df_synthetic = pd.merge(df_synthetic, df_population)
    df_synthetic_medians = pd.merge(
        df_synthetic[["commune_id", "household_income"]].groupby("commune_id").median().reset_index(),
        df_synthetic[["commune_id"]].groupby("commune_id").size().reset_index(name = "synthetic_observations")
        )
    df_synthetic_medians = df_synthetic_medians.rename({"household_income" : "synthetic_median"}, axis = 1)

    df_medians = pd.merge(df_filo_medians, df_synthetic_medians)
    df_medians["synthetic_median"] *= 12

    analysis["medians_by_commune"] = df_medians

    return analysis
