import numpy as np
import analysis.constants as c
from collections import OrderedDict
import data.constants as cd
import pandas as pd

def configure(context, require):
    require.stage("data.hts")
    require.stage("data.census.cleaned")
    require.stage("population.sociodemographics")

def extract_analysis_data(df):
    analysis = OrderedDict()

    # Total count
    analysis["total"] = np.sum(df["weight"])

    # Age classes
    df["age_class"] = np.digitize(df["age"], c.AGE_CLASS_BOUNDARIES, right = True)

    for k in range(c.AGE_CLASS_COUNT):
        analysis["age_class_%d" % k] = np.sum(df.loc[df["age_class"] == k, "weight"])

    # Employment
    analysis["employed_true"] = np.sum(df.loc[df["employed"], "weight"])
    analysis["employed_false"] = np.sum(df.loc[~df["employed"], "weight"])

    # License ownership
    if "has_license" in df.columns:
        analysis["license_true"] = np.sum(df.loc[df["has_license"], "weight"])
        analysis["license_false"] = np.sum(df.loc[~df["has_license"], "weight"])

    # Sex
    analysis["sex_male"] = np.sum(df.loc[df["sex"] == "male", "weight"])
    analysis["sex_female"] = np.sum(df.loc[df["sex"] == "female", "weight"])

    # PT Subscription
    if "has_pt_subscription" in df.columns:
        analysis["pt_subscription_true"] = np.sum(df.loc[df["has_pt_subscription"], "weight"])
        analysis["pt_subscription_false"] = np.sum(df.loc[~df["has_pt_subscription"], "weight"])

    # Number of vehicles
    df["number_of_vehicles_class"] = np.digitize(df["number_of_vehicles"], c.NUMBER_OF_VEHICLES_BOUNDARIES, right = True)

    for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT):
        analysis["number_of_vehicles_class_%d" % k] = np.sum(df.loc[df["number_of_vehicles_class"] == k, "weight"])

    # Number of bikes
    if "number_of_bikes" in df.columns:
        df["number_of_bikes_class"] = np.digitize(df["number_of_bikes"], c.NUMBER_OF_VEHICLES_BOUNDARIES, right = True)

        for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT):
            analysis["number_of_bikes_class_%d" % k] = np.sum(df.loc[df["number_of_bikes_class"] == k, "weight"])

    # Houeshold size
    df["household_size_class"] = np.digitize(df["household_size"], c.HOUSEHOLD_SIZE_CLASS_BOUNDARIES, right = True)

    for k in range(c.HOUSEHOLD_SIZE_CLASS_COUNT):
        analysis["household_size_class_%d" % k] = np.sum(df.loc[df["household_size_class"] == k, "weight"])

    return analysis

def extract_departements(df_hts, df_census, df_synthetic):
    return {
        departement_id : {
            "hts" : extract_analysis_data(pd.DataFrame(df_hts[df_hts["departement_id"] == departement_id], copy = True)),
            "census" : extract_analysis_data(pd.DataFrame(df_census[df_census["departement_id"] == departement_id], copy = True)),
            "synthetic" : extract_analysis_data(pd.DataFrame(df_synthetic[df_synthetic["departement_id"] == departement_id], copy = True)),
        }
        for departement_id in df_census["departement_id"].unique()
    }

def execute(context):
    df_hts = context.stage("data.hts")[0]
    df_census = context.stage("data.census.cleaned")
    df_synthetic = context.stage("population.sociodemographics")

    factor = context.config["input_downsampling"] if "input_downsampling" in context.config else 1.0
    df_synthetic["weight"] = 1.0 / factor

    return {
        "hts" : extract_analysis_data(df_hts),
        "census" : extract_analysis_data(df_census),
        "synthetic" : extract_analysis_data(df_synthetic),
        "by_departement": extract_departements(df_hts, df_census, df_synthetic)
    }
