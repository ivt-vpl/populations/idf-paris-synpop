import gzip
import numpy as np
import pandas as pd
import pandas.io.sql as pd_sql
import sqlite3
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import palettable

def configure(context, require):
    require.stage("analysis.data.flow")

def execute(context):
    df_comparison = context.stage("analysis.data.flow")

    df_comparison["name"] = df_comparison["origin_id"].astype(str) + " -> " + df_comparison["destination_id"].astype(str)
    df_comparison["name_x"] = df_comparison["origin_id"].astype(str) + " \n " + df_comparison["destination_id"].astype(str)
    df_comparison = df_comparison.sort_values(by = ["reference_count"])

    plt.figure(dpi = 300, figsize = (8,11))
    colors = palettable.colorbrewer.qualitative.Set2_5.mpl_colors

    plt.barh(np.arange(len(df_comparison)), df_comparison["reference_count"].values, height = 0.4, label = "Reference (Uncorrected)", color = colors[0], alpha = 0.25)
    plt.barh(np.arange(len(df_comparison)), df_comparison["corrected_reference_count"].values, height = 0.4, label = "Reference (Corrected)", color = colors[0])
    plt.barh(np.arange(len(df_comparison)) + 0.4, df_comparison["synthetic_count"].values, height = 0.4, label = "Simulation", color = colors[1])

    plt.gca().yaxis.set_major_locator(tck.FixedLocator(np.arange(len(df_comparison))))
    plt.gca().yaxis.set_major_formatter(tck.FixedFormatter(df_comparison["name"].values))

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(np.arange(100) * 1e3 * 100))
    plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(lambda x,p: "%dk" % (x * 1e-3)))
    plt.xlabel("Commute trips")

    plt.ylim([-1, len(df_comparison) + 1])
    plt.legend(loc = "best")
    plt.tight_layout()

    plt.savefig("%s/departement_flows_bar.pdf" % context.cache_path)

    plt.figure(dpi = 300, figsize = (4, 4))
    plt.plot([0, 1e6], [0, 1e6], 'k--', linewidth = 1)

    x = np.linspace(0, 1e6, 100)
    plt.fill_between(x, 0.8 * x, 1.2 * x, color = "k", alpha = 0.1, linewidth = 0, label = "+/- 20%")

    plt.plot(
        df_comparison["synthetic_count"].values,
        df_comparison["corrected_reference_count"].values,
        marker = ".", markersize = 3, linestyle = "none",
        label = "Reference (corrected)", color = colors[0]
    )

    plt.plot(
        df_comparison["synthetic_count"].values,
        df_comparison["reference_count"].values,
        marker = ".", markersize = 3, linestyle = "none", alpha = 0.25,
        label = "Reference (uncorrected)", color = colors[1]
    )

    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')

    plt.xlabel("Synthetic commute trips")
    plt.ylabel("Corrected reference commute trips")

    plt.legend(loc = "best")
    plt.tight_layout()

    plt.savefig("%s/departement_flows_scatter.pdf" % context.cache_path)
