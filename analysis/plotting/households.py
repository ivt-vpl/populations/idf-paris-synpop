import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import numpy as np
import analysis.constants as c
import palettable

def configure(context, require):
    require.stage("analysis.data.households")

def execute(context):
    analysis = context.stage("analysis.data.households")

    plt.figure(dpi = 120, figsize = (6, 8))

    labels = []
    dimensions = []

    labels += ["Cars: %s" % c.NUMBER_OF_VEHICLES_CLASS_NAMES[k] for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT)]
    dimensions += ["number_of_vehicles_class_%d" % k for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT)]

    labels += ["Bikes: %s" % c.NUMBER_OF_VEHICLES_CLASS_NAMES[k] for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT)]
    dimensions += ["number_of_bikes_class_%d" % k for k in range(c.NUMBER_OF_VEHICLES_CLASS_COUNT)]

    labels += ["Household Size: %s" % c.HOUSEHOLD_SIZE_CLASS_NAMES[k] for k in range(c.HOUSEHOLD_SIZE_CLASS_COUNT)]
    dimensions += ["household_size_class_%d" % k for k in range(c.HOUSEHOLD_SIZE_CLASS_COUNT)]

    values_synthetic = [analysis["synthetic"][d] if d in analysis["synthetic"] else 0 for d in dimensions]
    values_census = [analysis["census"][d] if d in analysis["census"] else 0 for d in dimensions]
    values_hts = [analysis["hts"][d] if d in analysis["hts"] else 0 for d in dimensions]

    values_synthetic = np.array(values_synthetic) / analysis["synthetic"]["total"]
    values_census = np.array(values_census) / analysis["census"]["total"]
    values_hts = np.array(values_hts) / analysis["hts"]["total"]

    y = np.arange(len(labels))
    colors = palettable.colorbrewer.qualitative.Set2_5.mpl_colors

    plt.barh(y + 0.125 + 0.0, values_census, height = 0.25, align = "edge", label = "Census", color = colors[0])
    plt.barh(y + 0.125 + 0.25, values_synthetic, height = 0.25, align = "edge", label = "Synthetic", color = colors[1])
    plt.barh(y + 0.125 + 0.5, values_hts, height = 0.25, align = "edge", label = "HTS", color = colors[2])

    for yi in y[::2]:
        plt.fill_between([0.0, 1.0], [yi, yi], [yi + 1.0, yi + 1.0], color = "black", alpha = 0.1, linewidth = 0)

    plt.gca().yaxis.set_major_locator(tck.FixedLocator(y + 0.5))
    plt.gca().yaxis.set_major_formatter(tck.FixedFormatter(labels))

    #plt.gca().xaxis.set_major_locator(tck.FixedLocator(np.arange(1, 10) * 1e-3))
    plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d%%" % (x * 100)))

    plt.xlabel("Share of (weighted) number of houesholds")
    plt.xlim([0, 0.75])

    plt.legend(loc = "best")

    plt.tight_layout()
    plt.savefig("%s/household_attributes.pdf" % context.cache_path)
