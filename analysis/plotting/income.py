import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import numpy as np
import analysis.constants as c
import palettable

def configure(context, require):
    require.stage("analysis.data.income")

def execute(context):
    analysis = context.stage("analysis.data.income")
    colors = palettable.colorbrewer.qualitative.Set2_5.mpl_colors

    # A) General income distribution
    plt.figure(dpi = 120, figsize = (6, 4))
    plt.plot(analysis["overall_income_distributions"]["egt"][1] * 1e-3, analysis["overall_income_distributions"]["egt"][0], marker = ".", label = "EGT", color = colors[2])
    plt.plot(analysis["overall_income_distributions"]["filo"][1] * 1e-3, analysis["overall_income_distributions"]["filo"][0], marker = ".", label = "FILOSOFI", color = colors[0])
    plt.plot(analysis["overall_income_distributions"]["synthetic"][1] * 1e-3, analysis["overall_income_distributions"]["synthetic"][0], marker = ".", label = "Synthetic", color = colors[1])
    plt.legend(loc = "best")
    plt.grid()
    plt.xlabel("Annual household income [1000 EUR]")
    plt.ylabel("Cumulative density")
    plt.tight_layout()
    plt.savefig("%s/income_cdf_region.pdf" % context.cache_path)

    # B) Comparison of medians
    minimum_income_observations = 1e3 * context.config["input_downsampling"]

    df = analysis["medians_by_commune"]
    df = df[df["synthetic_observations"] >= minimum_income_observations]

    maximum = 50
    minimum = 10

    plt.figure(dpi = 120, figsize = (6, 4))
    plt.plot(df["filo_median"] * 1e-3, df["synthetic_median"] * 1e-3, marker = ".", linestyle = "none", color = colors[0])
    x = np.linspace(0, maximum)
    plt.fill_between(x, x - 0.1 * x, x + 0.1 * x, color = "k", alpha = 0.2, linewidth = 0)
    plt.xlabel("FILOSOFI median income [1000 EUR]")
    plt.ylabel("Synthetic median income [1000 EUR]")
    plt.xlim([minimum, maximum])
    plt.ylim([minimum, maximum])
    plt.tight_layout()
    plt.savefig("%s/income_medians.pdf" % context.cache_path)













    #
