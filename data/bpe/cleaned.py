import simpledbf
from tqdm import tqdm
import pandas as pd
import numpy as np

def configure(context, require):
    require.stage("data.bpe.raw")
    require.stage("data.spatial.zones")

def execute(context):
    df = pd.read_hdf(context.stage("data.bpe.raw"))

    # Police, post office, etc ...
    df.loc[df["typequ"].str.startswith("A"), "activity_type"] = "other"

    # Restaurant
    df.loc[df["typequ"] == "A504", "activity_type"] = "leisure"

    # Shopping
    df.loc[df["typequ"].str.startswith("B"), "activity_type"] = "shop"

    # Education
    # (C702 "Restaurant universitaire" is interesting, is it leisure? ;))
    df.loc[df["typequ"].str.startswith("C"), "activity_type"] = "education"

    # Health
    df.loc[df["typequ"].str.startswith("D"), "activity_type"] = "other"

    # Transport
    df.loc[df["typequ"].str.startswith("E"), "activity_type"] = "other"

    # Sports & Culture
    df.loc[df["typequ"].str.startswith("F"), "activity_type"] = "leisure"

    # Tourism, hotels, etc. (Hôtel = G102)
    df.loc[df["typequ"].str.startswith("G"), "activity_type"] = "other"

    df["iris_id"] = df["dciris"].str.replace("_", "")
    f = df["depcom"] == df["iris_id"]
    df.loc[f, "iris_id"] = df.loc[f, "iris_id"] + "0000"

    df["x"] = df["lambert_x"].astype(np.float)
    df["y"] = df["lambert_y"].astype(np.float)

    # Cleanup
    df = df[["iris_id", "activity_type", "x", "y"]]
    df.loc[:, "bpe_id"] = np.arange(len(df))
    df["iris_id"] = df["iris_id"].astype(np.int)

    df_zones = context.stage("data.spatial.zones")[["iris_id", "zone_id", "commune_id"]]
    df = pd.merge(df, df_zones, on = "iris_id", how = "left")

    f = df["zone_id"].isna()
    print("Removing %d/%d enterprises from census because spatial zone is not known (anymore?)" % (sum(f), len(df)))
    df = df[~f]

    df["activity_type"] = df["activity_type"].astype("category")

    initial_count = len(df)
    df = df.dropna()
    final_count = len(df)

    print("Removing %d/%d (%.2f%%) enterprises because of NaN values" % (
        initial_count - final_count, initial_count, 100.0 * (1 - final_count / initial_count)
    ))

    df["commune_id"] = df["commune_id"].astype(np.int)
    return df[["bpe_id", "activity_type", "commune_id", "x", "y"]]
