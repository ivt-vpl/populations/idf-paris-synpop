import simpledbf
from tqdm import tqdm
import pandas as pd

def configure(context, require):
    pass

COLUMNS = [
    "dciris", "lambert_x", "lambert_y",
    "reg", "typequ", "depcom"
]

def execute(context):
    table = simpledbf.Dbf5("%s/bpe/bpe_ensemble_xy.dbf" % context.config["raw_data_path"], codec = "latin1")
    records = []

    with tqdm(total = 2416342, desc = "Reading enterprise census ...") as progress:
        with tqdm(desc = "Recorded enterprises") as recorded:
            for df_chunk in table.to_dataframe(chunksize = 10240):
                progress.update(len(df_chunk))
                df_chunk = df_chunk[df_chunk["reg"] == "11"]
                df_chunk = df_chunk[COLUMNS]

                if len(df_chunk) > 0:
                    records.append(df_chunk)
                    recorded.update(len(df_chunk))

    pd.concat(records).to_hdf("%s/data.hdf" % context.cache_path, "enterprises")
    return "%s/data.hdf" % context.cache_path
