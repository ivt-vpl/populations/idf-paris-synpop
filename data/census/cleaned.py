from tqdm import tqdm
import pandas as pd
import numpy as np

def configure(context, require):
    require.stage("data.census.raw")
    require.config("random_seed", 1234)

def execute(context):
    df = pd.read_hdf(context.stage("data.census.raw"))
    #df = pd.read_hdf("/home/sebastian/temp.hdf")

    # Construct proper household IDs
    df_household_ids = df[["CANTVILLE", "NUMMI"]]
    df_household_ids = df_household_ids[df_household_ids["NUMMI"] != "Z"]
    df_household_ids["temporary"] = df_household_ids["CANTVILLE"] + df_household_ids["NUMMI"]
    df_household_ids = df_household_ids.groupby("temporary").first()
    new_household_ids = np.arange(len(df_household_ids))
    df_household_ids["household_id"] = new_household_ids

    # Merge houeshold IDs in
    df = pd.merge(df, df_household_ids, on = ["CANTVILLE", "NUMMI"], how = "left")

    # Fill up undefined household ids
    f = np.isnan(df["household_id"])
    df.loc[f, "household_id"] = np.arange(np.count_nonzero(f)) + df["household_id"].max()
    df["household_id"] = df["household_id"].astype(np.int)

    # Put person IDs
    df.loc[:, "person_id"] = np.arange(len(df))

    # Clean IRIS
    df["iris_id"] = df["IRIS"]
    df["triris_id"] = df["TRIRIS"]
    df["commune_id"] = df["iris_id"].str[:5]
    df["region_id"] = df["REGION"]

    # Clean age
    df["age"] = df["AGED"].apply(lambda x: "0" if x == "000" else x.lstrip("0")).astype(np.int)

    # Clean departement
    df["departement_id"] = df["DEPT"].astype(np.int)

    # Clean COUPLE
    df["couple"] = df["COUPLE"] == "1"

    # Clean TRANS
    df.loc[df["TRANS"] == "1", "commute_mode"] = np.nan
    df.loc[df["TRANS"] == "2", "commute_mode"] = "walk"
    df.loc[df["TRANS"] == "3", "commute_mode"] = "bike"
    df.loc[df["TRANS"] == "4", "commute_mode"] = "car"
    df.loc[df["TRANS"] == "5", "commute_mode"] = "pt"
    df.loc[df["TRANS"] == "Z", "commute_mode"] = np.nan
    df["commute_mode"] = df["commute_mode"].astype("category")

    # Weight
    df.loc[:, "weight"] = df["IPONDI"].astype(np.float)

    # Clean SEXE
    df.loc[df["SEXE"] == "1", "sex"] = "male"
    df.loc[df["SEXE"] == "2", "sex"] = "female"
    df["sex"] = df["sex"].astype("category")

    # Clean nationality
    df.loc[df["INATC"] == "1", "nationality"] = "french"
    df.loc[df["INATC"] == "2", "nationality"] = "other"
    df["nationality"] = df["nationality"].astype("category")

    # Clean employment
    df["employed"] = df["TACT"] == "11"

    # Marital status
    df["married"] = df["STAT_CONJ"] == "A"

    # Studies
    df["studies"] = df["ETUD"] == "1"

    # Houeshold type
    df.loc[:, "household_type"] = "other"
    df.loc[df["TYPMC"] == "1", "household_type"] = "single"
    df.loc[df["TYPMC"] == "3", "household_type"] = "single_family"
    df.loc[df["TYPMC"] == "4", "household_type"] = "family"
    df["household_type"] = df["household_type"].astype("category")

    # Number of vehicles
    df["number_of_vehicles"] = df["VOIT"].apply(
        lambda x: str(x).replace("Z", "0").replace("X", "0")
    ).astype(np.int)

    df["number_of_vehicles"] += df["DEROU"].apply(
        lambda x: str(x).replace("U", "0").replace("Z", "0").replace("X", "0")
    ).astype(np.int)

    # Household size
    df_size = df[["household_id"]].groupby("household_id").size().reset_index(name = "household_size")
    df = pd.merge(df, df_size)

    # Household strcture
    df["household_structure"] = df["SFM"]
    df.loc[df["household_structure"] == "ZZ", "household_structure"] = "70"

    # Optionally remove houesholds
    if "debug_household_count" in context.config:
        count = context.config["debug_household_count"]
        np.random.seed(context.config["random_seed"])
        remaining_ids = np.random.choice(new_household_ids, size = count)
        print("DEBUG: Sampling down census to %d houesholds" % count)
        df = df[df["household_id"].isin(remaining_ids)]

    # We remove people who study or work in another region
    initial_count = len(np.unique(df["household_id"]))
    remove_ids = set(np.unique(df.loc[
        ~(df["ILETUD"].isin(("1", "2", "3", "Z")) |
        df["ILT"].isin(("1", "2", "3", "Z"))),
        "household_id"
    ]))
    df = df[~df["household_id"].isin(remove_ids)]
    print(
        "Removed %d/%d (%.2f%%) households because at least one person is working outside of Île-de-France" % (
        len(remove_ids), initial_count, 100.0 * len(remove_ids) / initial_count
    ))

    return df[[
        "person_id", "household_id", "weight",
        "iris_id", "triris_id", "departement_id",
        "age", "sex", "couple", "married",
        "commute_mode", "nationality", "employed",
        "studies", "household_type", "number_of_vehicles", "household_size",
        "commune_id", "region_id", "household_structure"
    ]]
