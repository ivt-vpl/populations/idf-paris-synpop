import simpledbf
from tqdm import tqdm
import pandas as pd

def configure(context, require):
    pass

COLUMNS = [
    "CANTVILLE", "NUMMI", "AGED",
    "APAF", "ARM", "COUPLE", "CS1",
    "DEPT", "DIPL_15", "ETUD", "ILETUD",
    "ILT", "INATC", "INPER", "INPERF",
    "IPONDI", "IRIS", "LIENF", "MOCO",
    "MODV", "REGION", "SEXE", "SFM",
    "STAT_CONJ", "TACT", "TRANS", "TRIRIS",
    "TYPFC", "TYPMC", "TYPMR", "VOIT", "DEROU"
]

def execute(context):
    table = simpledbf.Dbf5("%s/census/FD_INDCVI_2014.dbf" % context.config["raw_data_path"])
    records = []

    with tqdm(total = 19908607, desc = "Reading census ...") as progress:
        with tqdm(desc = "Recorded persons") as recorded:
            for df_chunk in table.to_dataframe(chunksize = 10240):
                progress.update(len(df_chunk))
                df_chunk = df_chunk[df_chunk["REGION"] == "11"]
                df_chunk = df_chunk[COLUMNS]

                if len(df_chunk) > 0:
                    records.append(df_chunk)
                    recorded.update(len(df_chunk))

    pd.concat(records).to_hdf("%s/census.hdf" % context.cache_path, "census")
    return "%s/census.hdf" % context.cache_path
