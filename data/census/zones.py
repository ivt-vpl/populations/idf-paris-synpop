import numpy as np
import pandas as pd

def configure(context, require):
    require.stage("data.census.cleaned")
    require.stage("data.spatial.zones")
    require.config("random_seed", 1234)

def execute(context):
    np.random.seed(context.config["random_seed"])
    
    df_census = context.stage("data.census.cleaned")
    df_zones = context.stage("data.spatial.zones")
    df_census = df_census.drop_duplicates("household_id")

    # Fix zones for samples without IRIS
    f_without_iris = df_census["iris_id"] == "ZZZZZZZZZ"
    df_without_iris = pd.DataFrame(df_census[f_without_iris], copy = True)
    print("Fixing %d zones for samples in communes without IRIS" % np.sum(f_without_iris))

    for departement_id in np.unique(df_without_iris["departement_id"]):
        df_candidates = pd.DataFrame(df_zones[
            ~df_zones["has_iris"] &
            (df_zones["zone_level"] == "commune") &
            (df_zones["departement_id"] == departement_id)
        ][["zone_id", "population", "au", "commune_id"]], copy = True)

        weights = df_candidates["population"].values
        weights /= np.sum(weights)

        f = df_without_iris["departement_id"] == departement_id
        unknowns = int(np.sum(f))

        indices = np.repeat(np.arange(weights.shape[0]), np.random.multinomial(int(np.sum(f)), weights))
        df_without_iris.loc[f, "zone_id"] = df_candidates.iloc[indices]["zone_id"].values
        df_without_iris.loc[f, "zone_au"] = df_candidates.iloc[indices]["au"].values
        df_without_iris.loc[f, "commune_id"] = df_candidates.iloc[indices]["commune_id"].values
        df_without_iris.loc[f, "iris_id"] = np.nan

    # Fix zones for samples in IRIS with low population
    f_small_iris = df_census["iris_id"].str.endswith("XXXX")
    df_small_iris = pd.DataFrame(df_census[f_small_iris], copy = True)
    df_small_iris["commune_id"] = df_small_iris["commune_id"].astype(np.int)
    print("Fixing %d zones for samples in small (<200 inhabitants) IRIS" % np.sum(f_small_iris))

    for commune_id in np.unique(df_small_iris["commune_id"]):
        df_candidates = pd.DataFrame(df_zones[
            (df_zones["zone_level"] == "iris") &
            (df_zones["commune_id"] == commune_id) &
            (df_zones["population"] <= 200)
        ][["zone_id", "population", "au", "iris_id"]], copy = True)
        assert(len(df_candidates) > 0)

        weights = df_candidates["population"].values
        weights /= np.sum(weights)

        f = df_small_iris["commune_id"] == commune_id
        unknowns = int(np.sum(f))

        indices = np.repeat(np.arange(weights.shape[0]), np.random.multinomial(int(np.sum(f)), weights))
        df_small_iris.loc[f, "zone_id"] = df_candidates.iloc[indices]["zone_id"].values
        df_small_iris.loc[f, "zone_au"] = df_candidates.iloc[indices]["au"].values
        df_small_iris.loc[f, "iris_id"] = df_candidates.iloc[indices]["iris_id"].values

    # All known IRIS
    f_known = ~(df_census["iris_id"] == "ZZZZZZZZZ") & ~df_census["iris_id"].str.endswith("XXXX")
    df_known = pd.DataFrame(df_census[f_known], copy = True)
    df_known["iris_id"] = df_known["iris_id"].astype(np.int)
    print("Imputing zone ID into %d samples with known IRIS" % np.sum(f_known))

    df_known = pd.merge(df_known, df_zones[
        df_zones["zone_level"] == "iris"
    ][["zone_id", "iris_id", "au"]], how = "left")
    df_known["zone_au"] = df_known["au"]

    assert(not df_known["zone_id"].isna().any())

    df_zones = pd.concat([
        df_without_iris[["household_id", "zone_id", "commune_id", "iris_id", "zone_au"]],
        df_small_iris[["household_id", "zone_id", "commune_id", "iris_id", "zone_au"]],
        df_known[["household_id", "zone_id", "commune_id", "iris_id", "zone_au"]]
    ])

    assert(len(df_census) == len(df_zones))

    # Data types
    df_zones["commune_id"] = df_zones["commune_id"].astype(np.int)

    return df_zones[["household_id", "zone_id", "commune_id", "iris_id", "zone_au"]]
