from tqdm import tqdm
import pandas as pd
import numpy as np
import data.constants as c

def configure(context, require):
    require.stage("data.egt.raw")
    require.stage("data.spatial.zones")

PURPOSE_MAP = {
    "home" : [1, 2, 3] + [34, 35],
    "work" : [11, 12, 13, 14],
    "leisure" : [15, 16, 17] + [41, 42, 43, 44, 45, 46, 47],
    "education" : [21, 22, 23, 24, 25, 26, 27],
    "shop" : [31, 32, 33],
    "other" : [50, 51, 52, 53, 54, 55] + [61, 62, 63, 64] + [98]
}

PURPOSE_MAP = {
    1 : "home",
    2 : "work",
    3 : "work",
    4 : "education",
    5 : "shop",
    6 : "other",
    7 : "other",
    8 : "leisure"
    # 9 : "other" # default
}

MODES_MAP = {
    1 : "pt",
    2 : "car",
    3 : "car_passenger",
    4 : "car",
    5 : "bike",
    #6 : "pt", # default (other)
    7 : "walk"
}

def execute(context):
    df_households, df_persons, df_trips = context.stage("data.egt.raw")

    # Filter out people without trip chains
    df = df_trips.drop_duplicates(["NQUEST", "NP"])[["NQUEST", "NP"]]
    df_persons = pd.merge(df_persons, df, on = ["NQUEST", "NP"], how = "inner")

    # Filter out irrelevant households
    df = df_persons.drop_duplicates("NQUEST")[["NQUEST"]]
    df_households = pd.merge(df_households, df, how = "inner")

    # Clean IDs
    df_households.loc[:, "egt_household_id"] = df_households.loc[:, "NQUEST"].astype(np.int)
    df_persons.loc[:, "egt_person_id"] = df_persons.loc[:, "NP"].astype(np.int)
    df_persons.loc[:, "egt_household_id"] = df_persons.loc[:, "NQUEST"].astype(np.int)
    df_trips.loc[:, "egt_person_id"] = df_trips.loc[:, "NP"].astype(np.int)
    df_trips.loc[:, "egt_household_id"] = df_trips.loc[:, "NQUEST"].astype(np.int)
    df_trips.loc[:, "egt_trip_id"] = df_trips.loc[:, "ND"].astype(np.int)

    df_persons = pd.DataFrame(df_persons, copy = True)

    # Construct IDs for households, persons and trips
    df_households.loc[:, "household_id"] = np.arange(len(df_households))

    df_persons = pd.merge(
        df_persons, df_households[["egt_household_id", "household_id"]],
        on = "egt_household_id"
    )
    df_persons.loc[:, "person_id"] = np.arange(len(df_persons))

    df_trips = pd.merge(
        df_trips, df_persons[["egt_person_id", "egt_household_id", "person_id", "household_id"]],
        on = ["egt_person_id", "egt_household_id"]
    )
    df_trips.loc[:, "trip_id"] = np.arange(len(df_trips))

    # Weight
    df_persons.loc[:, "weight"] = df_persons["POIDSP"].astype(np.float)
    df_households.loc[:, "household_weight"] = df_households["POIDSM"].astype(np.float)

    # Clean age
    df_persons.loc[:, "age"] = df_persons["AGE"]

    # Clean departement
    df_persons["departement_id"] = df_persons["RESDEP"].astype(np.int)
    df_households["departement_id"] = df_households["RESDEP"].astype(np.int)

    # Clean SEXE
    df_persons.loc[df_persons["SEXE"] == 1, "sex"] = "male"
    df_persons.loc[df_persons["SEXE"] == 2, "sex"] = "female"
    df_persons["sex"] = df_persons["sex"].astype("category")

    # Clean employment
    df_persons["employed"] = df_persons["OCCP"].isin([1.0, 2.0])

    # Household size
    df_households = pd.merge(
        df_households,
        df_persons[["household_id"]].groupby("household_id").size().reset_index(name = "household_size"),
        how = "inner"
    )

    # Studies
    df_persons["studies"] = df_persons["OCCP"].isin([3.0, 4.0, 5.0])

    # Number of vehicles
    df_households["number_of_vehicles"] = df_households["NB_2RM"] + df_households["NB_VD"]
    df_households["number_of_bikes"] = df_households["NB_VELO"]

    # License
    df_persons["has_license"] = (df_persons["PERMVP"] == 1) | (df_persons["PERM2RM"] == 1)

    # Household income
    df_households.loc[:, "household_income_class"] = df_households.loc[:, "REVENU"] - 1
    df_households.loc[df_households["REVENU"].isin([11.0, 12.0]), "household_income_class"] = np.nan

    # Has subscription
    df_persons["has_pt_subscription"] = df_persons["ABONTC"] > 1

    ## Trips

    df_trips["purpose"] = "other"
    for category, purpose in PURPOSE_MAP.items():
        df_trips.loc[df_trips["DESTMOT_H9"] == category, "purpose"] = purpose
        df_trips.loc[df_trips["ORMOT_H9"] == category, "preceeding_purpose"] = purpose
    df_trips["purpose"] = df_trips["purpose"].astype("category")
    df_trips["preceeding_purpose"] = df_trips["preceeding_purpose"].astype("category")

    df_trips["mode"] = "pt"
    for category, mode in MODES_MAP.items():
        df_trips.loc[df_trips["MODP_H7"] == category, "mode"] = mode
    df_trips["mode"] = df_trips["mode"].astype("category")

    df_trips["weekday"] = True
    df_trips["departure_time"] = df_trips["ORH"] * 3600.0 + df_trips["ORM"] * 60.0
    df_trips["arrival_time"] = df_trips["DESTH"] * 3600.0 + df_trips["DESTM"] * 60.0
    df_trips["crowfly_distance"] = df_trips["DPORTEE"] * 1000.0

    df_trips["origin_commune_id"] = df_trips["ORCOMM"]
    df_trips["destination_commune_id"] = df_trips["DESTCOMM"]
    df_households["commune_id"] = df_households["RESCOMM"]

    # Impute AU
    df_zones = context.stage("data.spatial.zones")
    df_zones = pd.DataFrame(df_zones[df_zones["zone_level"] == "commune"], copy = True)
    df_zones = df_zones[["commune_id", "au"]]
    df_zones["commune_id"] = df_zones["commune_id"].astype(np.int)

    df_zones.columns = ["origin_commune_id", "origin_au"]
    df_trips = pd.merge(df_trips, df_zones, on = "origin_commune_id", how = "left")

    df_zones.columns = ["destination_commune_id", "destination_au"]
    df_trips = pd.merge(df_trips, df_zones, on = "destination_commune_id", how = "left")

    df_zones.columns = ["commune_id", "zone_au"]
    df_households = pd.merge(df_households, df_zones, on = "commune_id", how = "left")

    # Impute activity duration
    df_duration = pd.DataFrame(df_trips[[
        "person_id", "trip_id", "arrival_time"
    ]], copy = True)

    df_following = pd.DataFrame(df_trips[[
        "person_id", "trip_id", "departure_time"
    ]], copy = True)
    df_following.columns = ["person_id", "trip_id", "following_trip_departure_time"]
    df_following["trip_id"] = df_following["trip_id"] - 1

    df_duration = pd.merge(df_duration, df_following, on = ["person_id", "trip_id"])
    df_duration["activity_duration"] = df_duration["following_trip_departure_time"] - df_duration["arrival_time"]
    df_duration.loc[df_duration["activity_duration"] < 0.0, "activity_duration"] += 24.0 * 3600.0

    df_duration = df_duration[["person_id", "trip_id", "activity_duration"]]
    df_trips = pd.merge(df_trips, df_duration, how = "left", on = ["person_id", "trip_id"])

    # Remove unuseable data
    remove_trip_ids = set()
    remove_household_ids = set()
    remove_person_ids = set()

    # Chain contains unknown origin or destination types
    f = np.isnan(df_trips["origin_au"]) | np.isnan(df_trips["destination_au"])
    print("Found %d trips that do not have origin or destination information" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Chain does not end with home
    df_trips = df_trips.sort_values(by = ["person_id", "trip_id"])
    df_last = df_trips.drop_duplicates("person_id", keep = "last")
    f = df_last["purpose"] != "home"
    print("Found %d ending trips which do not lead to home activity" % sum(f))
    remove_trip_ids |= set(df_last.loc[f, "trip_id"])

    # Chain does not start with home
    df_trips = df_trips.sort_values(by = ["person_id", "trip_id"])
    df_first = df_trips.drop_duplicates("person_id", keep = "first")
    f = df_first["preceeding_purpose"] != "home"
    print("Found %d starting trips which do not start at home" % sum(f))
    remove_trip_ids |= set(df_first.loc[f, "trip_id"])

    # Chain contains an unknown mode
    f = df_trips["mode"].isna()
    print("Found %d trips with unknown mode" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Chain contains unknown crowfly distance
    f = df_trips["crowfly_distance"].isna()
    print("Found %d trips with unknown crowfly distance" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Chain contains invalid duration
    f = ~df_trips["activity_duration"].isna() & df_trips["activity_duration"] < 0.0
    print("Found %d trips with negative activity duration" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Chain contains unknown departure time
    f = df_trips["departure_time"].isna()
    print("Found %d trips with unknown departure time" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Chain contains a trip going outside of Île-de-France
    df_trips["origin_departement"] = np.floor(df_trips["origin_commune_id"] / 1000).astype(np.int)
    df_trips["destination_departement"] = np.floor(df_trips["destination_commune_id"] / 1000).astype(np.int)

    f = ~df_trips["origin_departement"].isin(c.IDF_DEPARTEMENTS_INT)
    f |= ~df_trips["destination_departement"].isin(c.IDF_DEPARTEMENTS_INT)

    print("Found %d trips not starting or ending in Île-de-France" % sum(f))
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    # Remove households with unknown Aire Urbaine
    f = df_households["zone_au"].isna()
    print("Found %d households with unknown Aire Urbaine" % sum(f))
    remove_household_ids |= set(df_households.loc[f, "household_id"])

    f = df_households["household_income_class"].isna()
    print("Found %d households with unknown income" % sum(f))
    remove_household_ids |= set(df_households.loc[f, "household_id"])

    # Remove things that get inconsistent
    f = df_trips["trip_id"].isin(remove_trip_ids)
    remove_person_ids |= set(df_trips.loc[f, "person_id"])

    # We do not make households consistent... since we're matching persons later
    #f = df_persons["person_id"].isin(remove_person_ids)
    #remove_household_ids |= set(df_persons.loc[f, "household_id"])

    # Remove top-down
    f = df_persons["household_id"].isin(remove_household_ids)
    remove_person_ids |= set(df_persons.loc[f, "person_id"])

    f = df_trips["person_id"].isin(remove_person_ids)
    remove_trip_ids |= set(df_trips.loc[f, "trip_id"])

    initial_households = len(df_households)
    initial_persons = len(df_persons)
    initial_trips = len(df_trips)

    df_trips = df_trips[~df_trips["trip_id"].isin(remove_trip_ids)]
    df_persons = df_persons[~df_persons["person_id"].isin(remove_person_ids)]
    df_households = df_households[~df_households["household_id"].isin(remove_household_ids)]

    final_households = len(df_households)
    final_persons = len(df_persons)
    final_trips = len(df_trips)

    print("= Removed %d/%d households (%.2f%%) with insufficient data" % (
        initial_households - final_households, initial_households, 100.0 * (1.0 - final_households / initial_households)
    ))

    print("= Removed %d/%d persons (%.2f%%) with insufficient data" % (
        initial_persons - final_persons, initial_persons, 100.0 * (1.0 - final_persons / initial_persons)
    ))

    print("= Removed %d/%d trips (%.2f%%) with insufficient data" % (
        initial_trips - final_trips, initial_trips, 100.0 * (1.0 - final_trips / initial_trips)
    ))

    # Select only relevant fields
    df_trips = df_trips[[
        "person_id", "trip_id", "weekday", "household_id",
        "purpose", "mode",
        "departure_time", "arrival_time",
        "crowfly_distance", "origin_au", "destination_au",
        "activity_duration"
    ]]

    df_persons = df_persons[[
        "person_id", "household_id",
        "departement_id",
        "age", "sex",
        "employed",
        "studies", "has_license",
        "has_pt_subscription", "weight"
        # "region", "couple", "married", "nationality",
    ]]

    df_households = df_households[[
        "household_id",
        "household_size",
        "number_of_vehicles", "number_of_bikes",
        "household_income_class", "zone_au", "household_weight"
        # "household_type",  "dogs", "cats"
    ]]

    # Merge household information into persons
    df_persons = pd.merge(df_persons, df_households, on = "household_id")

    # Weekend information (EGT has it filtered already, ENTD did not!)
    df_persons["has_weekday_trips"] = True
    df_persons["has_weekend_trips"] = False
    df_trips["day_id"] = 1 # EGT has only one day per person

    # Primary activity information
    df_education = df_trips[df_trips["purpose"] == "education"][["person_id"]].drop_duplicates()
    df_education["has_education_trip"] = True
    df_persons = pd.merge(df_persons, df_education, how = "left")
    df_persons["has_education_trip"] = df_persons["has_education_trip"].fillna(False)

    df_work = df_trips[df_trips["purpose"] == "work"][["person_id"]].drop_duplicates()
    df_work["has_work_trip"] = True
    df_persons = pd.merge(df_persons, df_work, how = "left")
    df_persons["has_work_trip"] = df_persons["has_work_trip"].fillna(False)

    # Find commute information
    df_commute = df_trips[df_trips["purpose"].isin(["work", "education"])]
    df_commute = df_commute.sort_values(by = ["person_id", "crowfly_distance"])
    df_commute = df_commute.drop_duplicates("person_id", keep = "last")

    df_commute = df_commute[["person_id", "crowfly_distance", "mode"]]
    df_commute.columns = ["person_id", "commute_distance", "commute_mode"]

    df_persons = pd.merge(df_persons, df_commute, on = "person_id", how = "left")

    assert(not df_persons[df_persons["has_work_trip"]]["commute_distance"].isna().any())
    assert(not df_persons[df_persons["has_education_trip"]]["commute_distance"].isna().any())

    return df_persons, df_trips
