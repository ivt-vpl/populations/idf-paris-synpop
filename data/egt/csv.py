from tqdm import tqdm
import pandas as pd
import numpy as np

def configure(context, require):
    require.stage("data.egt.cleaned")

def execute(context):
    df_persons, df_trips = context.stage("data.egt.cleaned")

    df_persons.to_csv("%s/persons.csv" % context.cache_path, sep = ";", index = None)
    df_trips.to_csv("%s/trips.csv" % context.cache_path, sep = ";", index = None)
