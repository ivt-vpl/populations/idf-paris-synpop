import simpledbf
from tqdm import tqdm
import pandas as pd

MENAGES_COLUMNS = [
    "RESDEP", "NQUEST", "POIDSM", "NB_VELO", "NB_VD", "REVENU", "RESCOMM",
    "NB_VEH", "NB_2RM"
] # Region missing compared to ENTD -> Assume EGT

PERSONNES_COLUMNS = [
    "RESDEP", "NP", "POIDSP", "NQUEST", "SEXE", "AGE", "PERMVP",
    "ABONTC", "OCCP", "PERM2RM"
]

DEPLACEMENTS_COLUMNS = [
    "NQUEST", "NP", "ND",
    "ORDEP", "DESTDEP", "ORH", "DESTH", "ORM", "DESTM", "ORCOMM", "DESTCOMM",
    "DPORTEE", "MODP_H7", "DESTMOT_H9", "ORMOT_H9"
]

def configure(context, require):
    pass

def execute(context):
    df_menages = pd.read_csv(
        "%s/egt/Menages_semaine.csv" % context.config["raw_data_path"],
        sep = ",", encoding = "latin1"
    )[MENAGES_COLUMNS]

    df_personnes = pd.read_csv(
        "%s/egt/Personnes_semaine.csv" % context.config["raw_data_path"],
        sep = ",", encoding = "latin1"
    )[PERSONNES_COLUMNS]

    df_deplacements = pd.read_csv(
        "%s/egt/Deplacements_semaine.csv" % context.config["raw_data_path"],
        sep = ",", encoding = "latin1"
    )[DEPLACEMENTS_COLUMNS]

    return df_menages, df_personnes, df_deplacements
