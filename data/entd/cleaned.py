from tqdm import tqdm
import pandas as pd
import numpy as np
import data.entd.raw

def configure(context, require):
    require.stage("data.entd.raw")

PURPOSE_MAP = [
    ("1", "home"),
    ("1.11", "education"),
    ("2", "shop"),
    ("3", "other"),
    ("4", "other"),
    ("5", "leisure"),
    ("6", "other"),
    ("7", "leisure"),
    ("8", "leisure"),
    ("9", "work")
]

MODES_MAP = [
    ("1", "walk"),
    ("2", "car"), #
    ("2.20", "bike"), # bike
    ("2.23", "car_passenger"), # motorcycle passenger
    ("2.25", "car_passenger"), # same
    ("3", "car"),
    ("3.32", "car_passenger"),
    ("4", "pt"), # taxi
    ("5", "pt"),
    ("6", "pt"),
    ("7", "pt"), # Plane
    ("8", "pt"), # Boat
#    ("9", "pt") # Other
]

def convert_time(x):
    return np.dot(np.array(x.split(":"), dtype = np.float), [3600.0, 60.0, 1.0])

def execute(context):
    df_individu, df_tcm_individu, df_menage, df_tcm_menage, df_deploc, df_comm = context.stage("data.entd.raw")

    # Filter out people without trip chains
    available_ids = np.unique(df_deploc["IDENT_IND"])
    df_individu = df_individu[df_individu["IDENT_IND"].isin(available_ids)]
    df_tcm_individu = df_tcm_individu[df_tcm_individu["IDENT_IND"].isin(available_ids)]

    # Filter out irrelevant households
    relevant_household_ids = np.unique(df_individu["idENT_MEN"])
    df_menage = df_menage[df_tcm_menage["idENT_MEN"].isin(relevant_household_ids)]
    df_tcm_menage = df_tcm_menage[df_tcm_menage["idENT_MEN"].isin(relevant_household_ids)]

    # Combine data sets
    Q_INDIVIDU_COLUMNS = set(data.entd.raw.Q_INDIVIDU_COLUMNS)
    Q_TCM_INDIVIDU_COLUMNS = set(data.entd.raw.Q_TCM_INDIVIDU_COLUMNS) - set(data.entd.raw.Q_INDIVIDU_COLUMNS)

    Q_MENAGE_COLUMNS = set(data.entd.raw.Q_MENAGE_COLUMNS)
    Q_TCM_MENAGE_COLUMNS = set(data.entd.raw.Q_TCM_MENAGE_COLUMNS) - set(data.entd.raw.Q_MENAGE_COLUMNS)

    df_persons = pd.merge(
        df_individu[list(Q_INDIVIDU_COLUMNS)],
        df_tcm_individu[list(Q_TCM_INDIVIDU_COLUMNS) + ["IDENT_IND"]],
        on = ["IDENT_IND"]
    )

    df_households = pd.merge(
        df_menage[list(Q_MENAGE_COLUMNS)],
        df_tcm_menage[list(Q_TCM_MENAGE_COLUMNS) + ["idENT_MEN"]],
        on = ["idENT_MEN"]
    )

    df_trips = df_deploc

    assert(len(df_persons) == len(df_individu))
    assert(len(df_households) == len(df_menage))

    # Clean IDs
    df_persons.loc[:, "person_id"] = df_persons.loc[:, "IDENT_IND"].astype(np.int)
    df_persons.loc[:, "household_id"] = df_persons.loc[:, "idENT_MEN"].astype(np.int)
    df_households.loc[:, "household_id"] = df_households.loc[:, "idENT_MEN"].astype(np.int)
    df_trips.loc[:, "person_id"] = df_trips.loc[:, "IDENT_IND"].astype(np.int)

    # Weight
    df_persons.loc[:, "weight"] = df_persons["POIDS_QIND1"].astype(np.float)

    # Clean age
    df_persons.loc[:, "age"] = df_persons["AGE"]

    # Clean departement
    df_persons["departement_id"] = df_persons["DEP"].astype(np.str).apply(
        lambda x: x.replace("A", "").replace("B", "").lstrip("0")
    ).astype(np.int)

    # Clean region
    df_persons["region"] = df_persons["RG"].astype(np.int)

    # Clean COUPLE
    df_persons["couple"] = df_persons["COUPLE"].isin(["1", "2"])

    # Clean SEXE
    df_persons.loc[df_persons["SEXE"] == 1, "sex"] = "male"
    df_persons.loc[df_persons["SEXE"] == 2, "sex"] = "female"
    df_persons["sex"] = df_persons["sex"].astype("category")

    # Clean nationality
    df_persons.loc[:, "nationality"] = "other"
    df_persons.loc[df_persons["INATIO"] == 1, "nationality"] = "french"
    df_persons.loc[df_persons["INATIO"] == 2, "nationality"] = "french"
    df_persons["nationality"] = df_persons["nationality"].astype("category")

    # Clean employment
    df_persons["employed"] = df_persons["SITUA"].isin([1, 2])

    # Marital status
    df_persons["married"] = df_persons["ETAMATRI"] == 2

    # Household size
    df_households["household_size"] = df_households["NPERS"]

    # Studies
    df_persons["studies"] = df_persons["ETUDES"] == 1

    # Household type
    df_households.loc[df_households["TYPMEN5"] == 1, "household_type"] = "single"
    df_households.loc[df_households["TYPMEN5"] == 2, "household_type"] = "single_family"
    df_households.loc[df_households["TYPMEN5"] == 3, "household_type"] = "family"
    df_households.loc[df_households["TYPMEN5"] == 4, "household_type"] = "family"
    df_households.loc[df_households["TYPMEN5"] == 5, "household_type"] = "other"
    df_households["household_type"] = df_households["household_type"].astype("category")

    # Number of vehicles
    df_households["number_of_vehicles"] = 0
    df_households["number_of_vehicles"] += df_households["V1_JNBVEH"]
    df_households["number_of_vehicles"] += df_households["V1_JNBMOTO"]
    df_households["number_of_vehicles"] += df_households["V1_JNBCYCLO"]
    #df_households["number_of_vehicles"] += df_households["V1_JNBAUTVEH"]
    #df_households["number_of_vehicles"] += df_households["V1_JNBCCVUL"]

    df_households["number_of_bikes"] = df_households["V1_JNBVELOADT"]

    # Animals ;)
    df_households["dogs"] = df_households["V1_LOGANIMOESP_A"] == 1
    df_households["cats"] = df_households["V1_LOGANIMOESP_B"] == 1

    # License
    df_persons["has_license"] = (df_persons["V1_GPERMIS"] == 1) | (df_persons["V1_GPERMIS2R"] == 1)

    # Household income
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("Moins de 400"), "household_income_class"] = 0
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 400"), "household_income_class"] = 1
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 600"), "household_income_class"] = 2
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 800"), "household_income_class"] = 3
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 1 000"), "household_income_class"] = 4
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 1 200"), "household_income_class"] = 5
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 1 500"), "household_income_class"] = 6
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 1 800"), "household_income_class"] = 7
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 2 000"), "household_income_class"] = 8
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 2 500"), "household_income_class"] = 9
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 3 000"), "household_income_class"] = 10
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 4 000"), "household_income_class"] = 11
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("De 6 000"), "household_income_class"] = 12
    df_households.loc[df_households["TrancheRevenuMensuel"].str.startswith("10 000"), "household_income_class"] = 13
    df_households["household_income_class"] = df_households["household_income_class"].astype(np.int)

    # Home location type
    df_households["zone_au"] = df_households["numcom_AU2010"]

    # Has subscription
    df_persons["has_pt_subscription"] = df_persons["V1_ICARTABON"] == 1

    ## Trips

    for prefix, activity_type in PURPOSE_MAP:
        df_trips.loc[
            df_trips["V2_MMOTIFDES"].astype(np.str).str.startswith(prefix), "purpose"
        ] = activity_type
    df_trips["purpose"] = df_trips["purpose"].astype("category")

    print("Found %d activities with NaN purpose (fixing to 'other')" % (np.count_nonzero(df_trips["purpose"].isna())))
    df_trips.loc[df_trips["purpose"].isna(), "purpose"] = "other"

    for prefix, mode in MODES_MAP:
        df_trips.loc[
            df_trips["V2_MTP"].astype(np.str).str.startswith(prefix), "mode"
        ] = mode
    df_trips["mode"] = df_trips["mode"].astype("category")

    df_trips["chain_starts_at_home"] = df_trips["V2_MORIDOM"] == 1
    df_trips["weekday"] = df_trips["V2_TYPJOUR"] == 1
    df_trips["departure_time"] = df_trips["V2_MORIHDEP"].apply(convert_time).astype(np.float)
    df_trips["arrival_time"] = df_trips["V2_MDESHARR"].apply(convert_time).astype(np.float)
    df_trips["routed_distance"] = df_trips["V2_MDISTTOT"] * 1000.0
    df_trips["commute_crowfly_distance"] = df_trips["V2_DVO_LOC"] * 1000.0
    df_trips["day_id"] = df_trips["IDENT_JOUR"]
    df_trips["origin_au"] = df_trips["v2_moricom_AU2010"]
    df_trips["destination_au"] = df_trips["v2_mdescom_AU2010"]
    df_trips["trip_id"] = df_trips["idENT_DEP"]

    print("Repairing departure times ...")
    shifted_count = 1

    while shifted_count > 0:
        df_times = pd.DataFrame(df_trips[["person_id", "day_id", "trip_id", "departure_time"]], copy = True)

        df_next = pd.DataFrame(df_times, copy = True)
        df_next.columns = ["person_id", "day_id", "trip_id", "previous_departure_time"]
        df_next["trip_id"] += 1

        df_times = pd.merge(df_times, df_next, on = ["person_id", "day_id", "trip_id"], how = "left")

        f = df_times["previous_departure_time"] > df_times["departure_time"]
        df_trips.loc[f, "departure_time"] += 24.0 * 3600.0
        shifted_count = np.count_nonzero(f)

        print("- Shifted departure times of %d trips to the next day" % shifted_count)

    print("Repairing arrival times ...")
    shifted_count = 1

    while shifted_count > 0:
        f = df_trips["arrival_time"] < df_trips["departure_time"]
        df_trips.loc[f, "arrival_time"] += 24.0 * 3600.0
        shifted_count = np.count_nonzero(f)

        print("- Shifted arrival times of %d trips to the next day" % shifted_count)

    df_trips = pd.DataFrame(df_trips[[
        "person_id", "day_id", "trip_id", "weekday",
        "chain_starts_at_home",
        "purpose", "mode",
        "departure_time", "arrival_time",
        "routed_distance", "origin_au", "destination_au", "commute_crowfly_distance"
    ]], copy = True)

    df_persons = df_persons[[
        "person_id", "household_id",
        "region", "departement_id",
        "age", "sex", "couple", "married",
        "employed", "nationality",
        "studies", "has_license",
        "has_pt_subscription", "weight"
    ]]

    df_households = df_households[[
        "household_id",
        "household_size", "household_type",
        "number_of_vehicles", "number_of_bikes",
        "household_income_class", "zone_au",
        "dogs", "cats"
    ]]

    df_persons = pd.merge(df_persons, df_households, on = "household_id")

    # Remove unuseable data
    initial_count = len(df_persons)
    df_trips["internal_id"] = np.arange(len(df_trips))
    remove_ids = set()

    # - unknown origin or destination types
    f = np.isnan(df_trips["origin_au"]) | np.isnan(df_trips["destination_au"])
    print("Found %d trips that do not have origin or destination information" % sum(f))
    remove_ids |= set(df_trips.loc[f, "internal_id"])

    # Chain does not end with home
    df_trips = df_trips.sort_values(by = ["person_id", "day_id", "trip_id", "internal_id"])
    df_last = df_trips.drop_duplicates(["person_id", "day_id"], keep = "last")
    f = df_last["purpose"] != "home"
    print("Found %d ending trips which do not lead to home activity" % sum(f))
    remove_ids |= set(df_last.loc[f, "internal_id"])

    # Chain does not start with home
    df_trips = df_trips.sort_values(by = ["person_id", "day_id", "trip_id", "internal_id"])
    df_first = df_trips.drop_duplicates(["person_id", "day_id"], keep = "first")
    f = ~df_first["chain_starts_at_home"]
    print("Found %d starting trips which do not start at home" % sum(f))
    remove_ids |= set(df_first.loc[f, "internal_id"])

    # Chian contains an unknown mode
    f = df_trips["mode"].isna()
    print("Found %d trips with unknown mode" % sum(f))
    remove_ids |= set(df_trips.loc[f, "internal_id"])

    # Chian contains unknown routed distance
    f = df_trips["routed_distance"].isna()
    print("Found %d trips with unknown routed distance" % sum(f))
    remove_ids |= set(df_trips.loc[f, "internal_id"])

    # Remove attached trips
    df_remove = df_trips[df_trips["internal_id"].isin(remove_ids)][[
        "person_id", "day_id"
    ]].drop_duplicates()
    df_remove["remove"] = True
    df_trips = pd.merge(df_trips, df_remove, on = ["person_id", "day_id"], how = "left")
    df_trips["remove"] = df_trips["remove"].fillna(False).astype(np.bool)
    df_trips = df_trips[~df_trips["remove"]]
    del df_trips["remove"]

    existing_person_ids = set(np.unique(df_persons["person_id"]))
    remaining_person_ids = set(np.unique(df_trips["person_id"]))
    removed_person_ids = existing_person_ids - remaining_person_ids

    df_persons = df_persons[df_persons["person_id"].isin(remaining_person_ids)]
    final_count = len(remaining_person_ids)

    print("= Removed %d/%d persons (%.2f%%) with insufficient trip data" % (
        initial_count - final_count, initial_count, 100.0 * (1.0 - final_count / initial_count)
    ))

    # Remove non-Île-de-France
    count_idf = np.sum(df_persons["region"] == 11)
    count_total = len(df_persons)

    print("(Île-de-France residents: %d/%d (%.2f%%), just for information, we do not filter for them!)" % (
        count_idf, count_total, 100.0 * count_idf / count_total
    ))

    # Impute weekend information
    weekday_ids = set(np.unique(df_trips.loc[df_trips["weekday"], "person_id"]))
    weekend_ids = set(np.unique(df_trips.loc[~df_trips["weekday"], "person_id"]))

    print("Observations with weekday: ", len(weekday_ids))
    print("Observations only weekday: ", len(weekday_ids - weekend_ids))
    print("Observations only weekend: ", len(weekend_ids - weekday_ids))

    df_persons["has_weekday_trips"] = df_persons["person_id"].isin(weekday_ids)
    df_persons["has_weekend_trips"] = df_persons["person_id"].isin(weekend_ids)

    # Impute activity duration
    df_duration = pd.DataFrame(df_trips[[
        "person_id", "day_id", "trip_id", "arrival_time"
    ]], copy = True)

    df_following = pd.DataFrame(df_trips[[
        "person_id", "day_id", "trip_id", "departure_time"
    ]], copy = True)
    df_following.columns = ["person_id", "day_id", "trip_id", "following_trip_departure_time"]
    df_following["trip_id"] = df_following["trip_id"] - 1

    df_duration = pd.merge(df_duration, df_following, on = ["person_id", "day_id", "trip_id"])
    df_duration["activity_duration"] = df_duration["following_trip_departure_time"] - df_duration["arrival_time"]
    df_duration.loc[df_duration["activity_duration"] < 0.0, "activity_duration"] += 24.0 * 3600.0

    df_duration = df_duration[["person_id", "day_id", "trip_id", "activity_duration"]]
    df_trips = pd.merge(df_trips, df_duration, how = "left", on = ["person_id", "day_id", "trip_id"])

    # Commute information
    #df_commute = df_commute.sort_values(by = ["IDENT_IND", "TYPLIEU"])

    #df_commute["commute_distance"] = df_commute["V1_BTRAVDIST"] * 1000.0
    #df_commute["commute_time"] = df_commute["V1_BTRAVTEMPSA"] * 60.0
    #df_commute["commute_return_time"] = df_commute["V1_BTRAVTEMPSA"] * 60.0
    #df_commute["person_id"] = df_commute["IDENT_IND"]

    #df_commute.loc[df_commute["TYPLIEU"] == 1, "commute_purpose"] = "work"
    #df_commute.loc[df_commute["TYPLIEU"] == 2, "commute_purpose"] = "work"
    #df_commute.loc[df_commute["TYPLIEU"] == 3, "commute_purpose"] = "education"
    #df_commute.loc[df_commute["TYPLIEU"] == 4, "commute_purpose"] = "education"
    #df_commute.loc[df_commute["TYPLIEU"] == 5, "commute_purpose"] = "education"
    #df_commute.loc[df_commute["TYPLIEU"] == 6, "commute_purpose"] = "education"

    #for prefix, mode in MODES_MAP:
    #    df_commute.loc[
    #        df_commute["V1_BTRAVMTP"].astype(np.str).str.startswith(prefix), "commute_mode"
    #    ] = mode
    #df_commute["commute_mode"] = df_commute["commute_mode"].astype("category")

    #df_commute = df_commute[[
    #    "person_id", "commute_distance", "commute_time", "commute_return_time", "commute_purpose", "commute_mode"
    #]].dropna()
    #df_commute = df_commute.drop_duplicates(["person_id", "commute_purpose"], keep = "first")

    #person_ids = set(np.unique(df_persons["person_id"]))
    #commute_ids = set(np.unique(df_commute["person_id"]))

    # Commute information for persons (consistent with EGT)
    df_commute = df_trips[df_trips["purpose"].isin(["work", "education"])]
    df_commute = df_commute.sort_values(by = ["person_id", "commute_crowfly_distance"])
    df_commute = df_commute.drop_duplicates("person_id", keep = "last")

    df_commute = df_commute[["person_id", "commute_crowfly_distance", "mode"]]
    df_commute.columns = ["person_id", "commute_distance", "commute_mode"]

    # In case we would want crowfly distance from ENTD directly (but seems to be same result)
    #df_commute = df_commute[["person_id", "mode"]]
    #df_commute.columns = ["person_id", "commute_mode"]

    #df_comm["person_id"] = df_comm["IDENT_IND"]
    #df_comm["commute_distance"] = df_comm["dvo_teg"] * 1000
    #df_comm = df_comm[["person_id", "commute_distance"]]
    #df_commute = pd.merge(df_commute, df_comm, how = "left")

    df_persons = pd.merge(df_persons, df_commute, on = "person_id", how = "left")

    return df_persons, df_trips #, df_commute
