import simpledbf
from tqdm import tqdm
import pandas as pd

Q_MENAGE_COLUMNS = [
    "DEP", "idENT_MEN", "PONDV1", "RG", #"V1_JNBVEH",
    "V1_JNBVELOADT", "V1_JNBVELOENF",
    "V1_LOGANIMOESP_A", "V1_LOGANIMOESP_B",
    "V1_JNBVEH", "V1_JNBMOTO", "V1_JNBCYCLO", "V1_JNBAUTVEH", "V1_JNBCCVUL"
]

Q_TCM_MENAGE_COLUMNS = [
    "NPERS", "PONDV1", "TrancheRevenuMensuel",
    "DEP", "idENT_MEN", "PONDV1", "RG",
    "TYPMEN5", "TYPMEN15", "numcom_AUCat", "numcom_AU2010",
    "numcom_UU2010", "numcom_UUCat", "numcom_zhu"
]

Q_INDIVIDU_COLUMNS = [
    "AGE", "CS24", "DEP", "DIP14", "IDENT_IND", "idENT_MEN",
    "MOB_ETUDES", "MOB_TRAVAIL", "POIDS_QIND1", "RG",
    "SEXE", "SITUA", "V1_GPERMIS", "V1_ICARTABON",
    "V1_TYPMREG", "V1_GPERMIS2R"
]

for i in range(1, 5):
    Q_INDIVIDU_COLUMNS += ["V1_ITYP%dCART" % i]
    for j in ["A", "B", "C", "D", "E", "F", "G"]:
        Q_INDIVIDU_COLUMNS += ["V1_ITYP%dRES_%s" % (i, j)]

Q_TCM_INDIVIDU_COLUMNS = [
    "AGE", "COUPLE", "CS24", "DEP", "DIP14", "ENFANT",
    "ETUDES", "IDENT_IND", "IDENT_MEN", "INATIO",
    "PONDV1", "SEXE", "SITUA", "rg", "ETAMATRI"
]

K_DEPLOC_COLUMNS = [
    "IDENT_IND", "V2_MMOTIFDES", "V2_MMOY1S", "V2_MORIDOM",
    "V2_TYPJOUR", "V2_MORIHDEP", "V2_MDESHARR", "V2_MDISTTOT",
    "IDENT_JOUR", "v2_moricom_AU2010", "v2_mdescom_AU2010", "idENT_DEP", "V2_MTP",
    "V2_DVO_LOC"
]

Q_IND_LIEU_TEG_COLUMNS = [
    "IDENT_IND", "V1_BTRAVDIST", "V1_BTRAVTEMPSA", "V1_BTRAVTEMPSR",
    "V1_BTRAVMTP", "IDENT_MEN", "idENT_LIEU", "TYPLIEU", "dvo_teg"
]

def configure(context, require):
    pass

def execute(context):
    df_individu = pd.read_csv(
        "%s/entd/Q_individu.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[Q_INDIVIDU_COLUMNS]

    df_tcm_individu = pd.read_csv(
        "%s/entd/Q_tcm_individu.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[Q_TCM_INDIVIDU_COLUMNS]

    df_menage = pd.read_csv(
        "%s/entd/Q_menage.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[Q_MENAGE_COLUMNS]

    df_tcm_menage = pd.read_csv(
        "%s/entd/Q_tcm_menage.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[Q_TCM_MENAGE_COLUMNS]

    df_deploc = pd.read_csv(
        "%s/entd/K_deploc.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[K_DEPLOC_COLUMNS]

    df_ind_lieu_teg = pd.read_csv(
        "%s/entd/Q_ind_lieu_teg.csv" % context.config["raw_data_path"],
        sep = ";", encoding = "latin1"
    )[Q_IND_LIEU_TEG_COLUMNS]

    return df_individu, df_tcm_individu, df_menage, df_tcm_menage, df_deploc, df_ind_lieu_teg
