from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck

def configure(context, require):
    require.stage("data.egt.cleaned")
    require.stage("data.entd.cleaned")

def create_classes(dfs, slot, bounds = None, values = None, postfix = "_class"):
    if not bounds is None:
        for df in dfs:
            df["%s%s" % (slot, postfix)] = np.digitize(df[slot], bounds, right = True)

        count = len(bounds)
    else:
        if values is None:
            values = set().union(*[set(df[slot].unique()) for df in dfs])

        for df in dfs:
            df["%s%s" % (slot, postfix)] = -1000

            for k, value in enumerate(values):
                df.loc[df[slot] == value, "%s%s" % (slot, postfix)] = k

        count = len(values)

    return {
        "slot": slot, "values": values, "bounds": bounds, "postfix": postfix, "count": count,
        "column": "%s%s" % (slot, postfix)
    }

def create_activity_chains(df_persons, df_trips, limit = 20):
    df_days = df_trips[["person_id", "day_id"]].drop_duplicates(keep = "first")
    df_trips = pd.DataFrame(pd.merge(df_trips, df_days, how = "inner"), copy = True)

    for long, short in { "home": "H", "other": "X", "education": "E", "leisure": "L", "shop": "S", "work": "W" }.items():
        df_trips.loc[df_trips["purpose"] == long, "chain"] = short

    df_chain = df_trips.groupby("person_id")["chain"].apply("".join).reset_index()
    df_chain["chain_length"] = df_chain["chain"].str.len()

    return df_chain

def limit_activity_chains(df_reference, dfs, limit = 20, consider_other = False):
    df_count = df_reference[["chain", "weight"]].groupby("chain").sum().reset_index().sort_values(by = "weight", ascending = False)
    limit_chains = df_count["chain"].values[:limit]

    for df in dfs:
        f = ~df["chain"].isin(limit_chains)
        df.loc[f, "chain"] = "other"
        df.loc[f, "chain_length"] = np.nan

    if consider_other:
        limit_chains = list(limit_chains) + ["other"]

    return limit_chains

def create_comparison(path, dfs, slot, labels = None):
    if labels is None: labels = ["%d" % (i + 1) for i in range(len(dfs))]

    # Prepare distributions
    distributions = [[
        np.sum(df[df["%s%s" % (slot["slot"], slot["postfix"])] == k]["weight"])
        for k in range(slot["count"])
    ] for df in dfs ]

    distributions = [d / np.sum(d) for d in distributions]

    # Plot distribution
    plt.figure(dpi = 300, figsize = (8, 5))

    bar_width = 0.9 / len(dfs)
    bar_offset = bar_width
    count = slot["count"]

    for i, (distribution, label) in enumerate(zip(distributions, labels)):
        plt.bar(np.arange(count) + bar_offset * i, distribution, width = bar_width, label = label, align = "edge")

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(np.arange(count) + 0.45))

    if not slot["bounds"] is None:
        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(["<= %.0f" % b for b in slot["bounds"]]))
    else:
        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter([str(v) for v in slot["values"]]))

    plt.legend(loc = "best")
    plt.grid()
    plt.title(slot["slot"])
    plt.savefig("%s/%s.png" % (path, slot["slot"]))

def execute(context):
    df_persons_egt, df_trips_egt = context.stage("data.egt.cleaned")
    df_persons_entd, df_trips_entd = context.stage("data.entd.cleaned")

    # Filter Île-de-France and other things for ENTD
    df_persons_entd = df_persons_entd[df_persons_entd["region"] == 11]
    df_persons_entd = df_persons_entd[df_persons_entd["has_weekday_trips"]]

    df_trips_entd = df_trips_entd[df_trips_entd["person_id"].isin(
        df_persons_entd["person_id"].unique()
    )]
    df_trips_entd = df_trips_entd[df_trips_entd["weekday"]]
    df_trips_entd = df_trips_entd[df_trips_entd["chain_starts_at_home"]]

    df_persons_entd = pd.DataFrame(df_persons_entd, copy = True)
    df_trips_entd = pd.DataFrame(df_trips_entd, copy = True)

    # Create activity chain information
    df_chains_egt = create_activity_chains(df_persons_egt, df_trips_egt)
    df_persons_egt = pd.merge(df_persons_egt, df_chains_egt, how = "left", on = "person_id")

    df_chains_entd = create_activity_chains(df_persons_entd, df_trips_entd)
    df_persons_entd = pd.merge(df_persons_entd, df_chains_entd, how = "left", on = "person_id")

    activity_chains = limit_activity_chains(df_persons_egt, [df_persons_egt, df_persons_entd])

    # Create person-level classes
    slots, dfs = [], [df_persons_egt, df_persons_entd]
    slots.append(create_classes(dfs, "age", bounds = [15, 18, 24, 45, 65, 80, np.inf]))
    slots.append(create_classes(dfs, "number_of_vehicles", bounds = [0, 1, 2, np.inf]))
    slots.append(create_classes(dfs, "sex"))
    slots.append(create_classes(dfs, "employed"))
    slots.append(create_classes(dfs, "studies"))
    slots.append(create_classes(dfs, "departement_id"))
    slots.append(create_classes(dfs, "household_size", bounds = [1, 2, 3, 4, np.inf]))
    slots.append(create_classes(dfs, "has_license"))
    slots.append(create_classes(dfs, "has_pt_subscription"))
    slots.append(create_classes(dfs, "number_of_bikes", bounds = [0, 1, 2, np.inf]))
    slots.append(create_classes(dfs, "commute_mode"))
    slots.append(create_classes(dfs, "commute_distance", bounds = np.array([1, 2, 5, 10, 20, 50, np.inf]) * 1000))

    # Chain attributes
    slots.append(create_classes(dfs, "chain", values = activity_chains))
    slots.append(create_classes(dfs, "chain_length", bounds = [1, 2, 3, 5, 7, np.inf]))

    df_persons_scaled = pd.DataFrame(df_persons_entd, copy = True)
    prior = 1e-3

    for i in tqdm(range(20), desc = "IPF"):
        for slot in slots:
                for k in range(slot["count"]):
                    f_reference = df_persons_egt[slot["column"]] == k
                    f_scaled = df_persons_scaled[slot["column"]] == k

                    reference_weight = np.sum(df_persons_egt.loc[f_reference, "weight"]) + prior
                    scaled_weight = np.sum(df_persons_scaled.loc[f_scaled, "weight"]) + prior

                    df_persons_scaled.loc[f_scaled, "weight"] *= reference_weight / scaled_weight

    # Plotting
    for slot in slots:
        create_comparison(context.cache_path, [
            df_persons_egt, df_persons_scaled, df_persons_entd
        ], slot, labels = ["EGT", "Scaled", "ENTD"])

    # Some additional analysis for trip-level attributes
    trip_slots, dfs = [], [df_trips_egt, df_trips_entd]
    trip_slots.append(create_classes(dfs, "purpose"))
    trip_slots.append(create_classes(dfs, "activity_duration", bounds = np.array([1, 2, 3, 4, 5, 6, np.inf]) * 3600))
    trip_slots.append(create_classes(dfs, "mode"))
    trip_slots.append(create_classes(dfs, "purpose"))
    trip_slots.append(create_classes(dfs, "departure_time", bounds = np.arange(5, 24) * 3600))
    trip_slots.append(create_classes(dfs, "arrival_time", bounds = np.arange(5, 24) * 3600))

    df_weighted_trips_egt = pd.merge(df_trips_egt, df_persons_egt[["person_id", "weight"]], how = "left", on = "person_id")
    df_weighted_trips_entd = pd.merge(df_trips_entd, df_persons_entd[["person_id", "weight"]], how = "left", on = "person_id")
    df_weighted_trips_scaled = pd.merge(df_trips_entd, df_persons_scaled[["person_id", "weight"]], how = "left", on = "person_id")

    for slot in trip_slots:
        create_comparison(context.cache_path, [
            df_weighted_trips_egt, df_weighted_trips_entd, df_weighted_trips_scaled
        ], slot, labels = ["EGT", "Scaled", "ENTD"])

    return df_persons_scaled, df_trips_entd
