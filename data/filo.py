import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.neighbors import KDTree

def configure(context, require):
    require.stage("data.spatial.zones")

IMC_OTHER = 0
IMC_SINGLE_MALE = 1
IMC_SINGLE_FEMALE = 2
IMC_COUPLE_WITHOUT_CHILDREN = 3
IMC_COUPLE_WITH_CHILDREN = 4
IMC_SINGLE_FAMILY = 5

IMC_STRUCTURE_CLASSES = [
    IMC_OTHER,
    IMC_SINGLE_MALE, IMC_SINGLE_FEMALE,
    IMC_COUPLE_WITHOUT_CHILDREN, IMC_COUPLE_WITH_CHILDREN,
    IMC_SINGLE_FAMILY
]

IMC_SIZE_CLASSES = [
    1, 2, 3, 4, 5
]

def impute_matching_clases(df_prior):
    df_prior["im_size"] = np.minimum(5, df_prior["household_size"])
    df_prior.loc[:, "im_structure"] = IMC_OTHER
    df_prior.loc[df_prior["household_structure"] == "11", "im_structure"] = IMC_SINGLE_MALE
    df_prior.loc[df_prior["household_structure"] == "12", "im_structure"] = IMC_SINGLE_FEMALE

    df_prior.loc[df_prior["household_structure"].str.startswith("3"), "im_structure"] = IMC_COUPLE_WITH_CHILDREN
    df_prior.loc[df_prior["household_structure"].str.startswith("5"), "im_structure"] = IMC_COUPLE_WITH_CHILDREN

    df_prior.loc[df_prior["household_structure"] == "30", "im_structure"] = IMC_COUPLE_WITHOUT_CHILDREN
    df_prior.loc[df_prior["household_structure"] == "51", "im_structure"] = IMC_COUPLE_WITHOUT_CHILDREN
    df_prior.loc[df_prior["household_structure"] == "52", "im_structure"] = IMC_COUPLE_WITHOUT_CHILDREN

    df_prior.loc[df_prior["household_structure"] == "22", "im_structure"] = IMC_SINGLE_FAMILY
    df_prior.loc[df_prior["household_structure"] == "40", "im_structure"] = IMC_SINGLE_FAMILY

def execute(context):
    distributions = {
        "structure" : {},
        "size" : {},
        "total" : {}
    }

    print("Loading income by municipality ...")
    df = pd.read_excel(
        "%s/filo/FILO_DISP_COM.xls" % context.config["raw_data_path"],
        sheet_name = "ENSEMBLE", skiprows = 5
    )[["CODGEO"] + ["D%d15" % q if q != 5 else "Q215" for q in range(1, 10)]]
    df.columns = ["commune_id", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9"]
    df["commune_id"] = pd.to_numeric(df["commune_id"], errors = "coerce")
    df = df.dropna()
    df["commune_id"] = df["commune_id"].astype(np.int)
    distributions["total"] = df

    for structure in tqdm(IMC_STRUCTURE_CLASSES[1:], desc = "Loading income by household structure"):
        df = pd.read_excel(
            "%s/filo/FILO_DISP_COM.xls" % context.config["raw_data_path"],
            sheet_name = "TYPMENR_%d" % structure, skiprows = 5
        )[["CODGEO"] + ["TYM%dD%d15" % (structure, q) if q != 5 else "TYM%dQ215" % structure for q in range(1, 10)]]
        df.columns = ["commune_id", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9"]
        df["commune_id"] = pd.to_numeric(df["commune_id"], errors = "coerce")
        df = df.dropna()
        df["commune_id"] = df["commune_id"].astype(np.int)
        distributions["structure"][structure] = df

    for size in tqdm(IMC_SIZE_CLASSES, desc = "Loading income by household size"):
        df = pd.read_excel(
            "%s/filo/FILO_DISP_COM.xls" % context.config["raw_data_path"],
            sheet_name = "TAILLEM_%d" % size, skiprows = 5
        )[["CODGEO"] + ["TME%dD%d15" % (size, q) if q != 5 else "TME%dQ215" % size for q in range(1, 10)]]
        df.columns = ["commune_id", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9"]
        df["commune_id"] = pd.to_numeric(df["commune_id"], errors = "coerce")
        df = df.dropna()
        df["commune_id"] = df["commune_id"].astype(np.int)
        distributions["size"][size] = df

    # Impute missing distributions by neirest neighbor
    df_communes = context.stage("data.spatial.zones")
    df_communes = df_communes[df_communes["zone_level"] == "commune"]

    print("Imputing income distributions by neirest neighbor ...")
    for dimension, values in [("total", (None,)), ("structure", IMC_STRUCTURE_CLASSES[1:]), ("size", IMC_SIZE_CLASSES)]:
        for value in values:
            df = distributions[dimension][value] if dimension != "total" else distributions["total"]

            existing_commune_ids = list(set(np.unique(df_communes["commune_id"])) & set(np.unique(df["commune_id"])))
            missing_commune_ids = list(set(np.unique(df_communes["commune_id"])) - set(np.unique(df["commune_id"])))

            f_existing = df_communes["commune_id"].isin(existing_commune_ids)
            df_existing = df_communes[f_existing]
            coordinates = np.vstack([df_existing["geometry"].centroid.x, df_existing["geometry"].centroid.y]).T
            kd_tree = KDTree(coordinates)

            f_missing = df_communes["commune_id"].isin(missing_commune_ids)
            df_missing = df_communes[f_missing]
            coordinates = np.vstack([df_missing["geometry"].centroid.x, df_missing["geometry"].centroid.y]).T
            indices = kd_tree.query(coordinates)[1].flatten()

            df_imputed = pd.concat([
                df[df["commune_id"] == df_existing.iloc[index]["commune_id"]] for index in indices
            ])
            df_imputed["commune_id"] = missing_commune_ids

            if dimension != "total":
                distributions[dimension][value] = pd.concat([df, df_imputed])
            else:
                distributions["total"] = pd.concat([df, df_imputed])

            print(dimension, "=", value, " -> ", len(df_imputed), "imputed")

    return distributions
