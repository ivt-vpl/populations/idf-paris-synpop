import numpy as np
import pandas as pd

def configure(context, require):
    pass

def execute(context):
    print("Loading income for Île-de-France ...")

    df = pd.read_excel(
        "%s/filo/region/FILO_DISP_REG.xls" % context.config["raw_data_path"],
        sheet_name = "ENSEMBLE", skiprows = 5
    )

    values = df[df["CODGEO"] == 11][[
        "D115", "D215", "D315", "D415", "Q215", "D615", "D715", "D815", "D915"
    ]].values[0]

    return values
