from tqdm import tqdm
import pandas as pd
import numpy as np

def configure(context, require):
    require.stage("data.egt.cleaned")
    require.stage("data.entd.cleaned")
    require.stage("data.entd.weighted")
    require.config("hts", "egt")

def execute(context):
    df_persons, df_trips = None, None

    if context.config["hts"] == "egt":
        df_persons, df_trips = context.stage("data.egt.cleaned")
    elif context.config["hts"] == "entd":
        df_persons, df_trips = context.stage("data.entd.cleaned")
    elif context.config["hts"] == "weighted_entd":
        df_persons, df_trips = context.stage("data.entd.weighted")

    df_passenger = pd.DataFrame(df_trips[["person_id", "mode"]], copy = True)
    df_passenger = df_passenger[df_passenger["mode"] == "car_passenger"][["person_id"]]
    df_passenger = df_passenger.drop_duplicates()
    df_passenger["is_passenger"] = True

    df_persons = pd.merge(df_persons, df_passenger, on = "person_id", how = "left")
    df_persons["is_passenger"] = df_persons["is_passenger"].fillna(False)
    df_persons["is_passenger"] = df_persons["is_passenger"].astype(np.bool)

    return df_persons, df_trips
