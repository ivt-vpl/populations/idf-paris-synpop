from tqdm import tqdm
import pandas as pd
import numpy as np
import simpledbf

def configure(context, require):
    pass

def execute(context):
    # First, load work

    table = simpledbf.Dbf5("%s/census/FD_MOBPRO_2015.dbf" % context.config["raw_data_path"])
    records = []

    with tqdm(desc = "Reading work flows ...", total = 7943392) as progress:
        with tqdm(desc = "Recorded movements") as recorded:
            for df_chunk in table.to_dataframe(chunksize = 10240):
                progress.update(len(df_chunk))
                f = df_chunk["REGION"] == "11"
                f |= df_chunk["REGLT"] == "11"
                df_chunk = df_chunk[f]
                df_chunk = df_chunk[["COMMUNE", "ARM", "TRANS", "IPONDI", "DCLT", "REGLT"]]

                if len(df_chunk) > 0:
                    records.append(df_chunk)
                    recorded.update(len(df_chunk))

    pd.concat(records).to_hdf("%s/work.hdf" % context.cache_path, "movements")

    # Second, load education

    table = simpledbf.Dbf5("%s/census/FD_MOBSCO_2015.dbf" % context.config["raw_data_path"])
    records = []

    with tqdm(desc = "Reading education flows ...", total = 4782736) as progress:
        with tqdm(desc = "Recorded movements") as recorded:
            for df_chunk in table.to_dataframe(chunksize = 10240):
                progress.update(len(df_chunk))
                f = df_chunk["REGION"] == "11"
                f |= df_chunk["REGETUD"] == "11"
                df_chunk = df_chunk[f]
                df_chunk = df_chunk[["COMMUNE", "ARM", "IPONDI", "DCETUF", "REGETUD"]]

                if len(df_chunk) > 0:
                    records.append(df_chunk)
                    recorded.update(len(df_chunk))

    pd.concat(records).to_hdf("%s/education.hdf" % context.cache_path, "movements")

    return (
        "%s/work.hdf" % context.cache_path,
        "%s/education.hdf" % context.cache_path
    )
