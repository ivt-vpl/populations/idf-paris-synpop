import numpy as np
import pandas as pd

def configure(context, require):
    pass

def execute(context):
    df_population = pd.read_excel(
        "%s/base-ic-evol-struct-pop-2014.xls" % context.config["raw_data_path"],
        skiprows = 5, sheet_name = "IRIS"
    )[["REG", "DEP", "COM", "IRIS", "P14_POP"]]

    df_population.columns = ["region_id", "departement_id", "commune_id", "iris_id", "population"]
    df_population = df_population[df_population["region_id"] == 11]
    
    return df_population
