import numpy as np
import pandas as pd
import data.constants

def configure(context, require):
    pass

def execute(context):
    df_au = pd.read_excel(
        "%s/AU2010 au 01-01-2018_V2.xls" % context.config["raw_data_path"],
        sheet_name = "Composition_communale", skiprows = 5
    )[["CODGEO", "CATAEU2010", "REG"]]
    df_au.columns = ["commune_id", "au", "region"]

    # Filter out everything outside of Île-de-France
    df_au = df_au[df_au["region"] == 11]

    return df_au[["commune_id", "au"]]
