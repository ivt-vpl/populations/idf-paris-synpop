import numpy as np
import pandas as pd
import geopandas as gpd
import data.constants

def configure(context, require):
    pass

def execute(context):
    df_iris = gpd.read_file("%s/iris/CONTOURS-IRIS.shp" % context.config["raw_data_path"])
    df_iris = df_iris[["INSEE_COM", "CODE_IRIS", "geometry"]]
    df_iris.columns = ["commune_id", "iris_id", "geometry"]
    df_iris.crs = {"init":"EPSG:2154"}

    df_iris["departement_id"] = df_iris["commune_id"].str[:2]

    df_iris = df_iris[
        df_iris["departement_id"].isin(data.constants.IDF_DEPARTEMENTS)
    ]

    return df_iris
