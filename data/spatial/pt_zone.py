import data.constants
import pandas as pd
import numpy as np
import geopandas as gpd
import shapely.geometry as geo
from sklearn.neighbors import KDTree

def configure(context, require):
    pass

def impute(df_target, df_zones, x = "x", y = "y", name = "pt_zone_id"):
    coordinates = np.vstack([df_zones.geometry.centroid.x, df_zones.geometry.centroid.y]).T
    kd_tree = KDTree(coordinates)

    coordinates = list(zip(df_target[x], df_target[y]))
    indices = kd_tree.query(coordinates, return_distance = False)

    df_target[name] = df_zones.iloc[indices.flatten()]["zone_id"].values

def execute(context):
    df_zones = pd.read_csv("%s/gtfs/stops.txt" % context.config["raw_data_path"])
    df_zones = df_zones[["stop_id", "stop_lat", "stop_lon", "zone_id"]].dropna()

    geometry = [geo.Point(x, y) for x, y in zip(df_zones["stop_lon"], df_zones["stop_lat"])]
    df_zones["geometry"] = geometry

    df_zones = df_zones[["stop_id", "zone_id", "geometry"]]
    df_zones = gpd.GeoDataFrame(df_zones)
    df_zones.crs = {"init" : "EPSG:4326"}

    df_zones = df_zones[df_zones["zone_id"].isin([1, 2, 3, 4, 5])]
    df_zones = df_zones.to_crs({"init" : "EPSG:2154"})
    df_zones["zone_id"] = df_zones["zone_id"].astype(np.int)

    return df_zones[["zone_id", "geometry"]]
