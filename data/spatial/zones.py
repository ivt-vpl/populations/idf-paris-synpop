import numpy as np
import pandas as pd
import geopandas as gpd
import data.constants
from sklearn.neighbors import KDTree

def configure(context, require):
    require.stage("data.population")
    require.stage("data.spatial.iris")
    require.stage("data.spatial.au")

def execute(context):
    df_population = context.stage("data.population")
    df_iris = context.stage("data.spatial.iris")
    assert(len(df_population) == len(df_iris))

    # Join population count and region into IRIS
    df_iris = pd.merge(df_iris, df_population[["iris_id", "population", "region_id"]])

    # Create geometries for communes
    df_communes = df_iris[[
        "commune_id", "geometry", "population"
    ]].dissolve(by = "commune_id", aggfunc = "sum").reset_index()
    df_communes["iris_id"] = df_communes["commune_id"] + "0000"

    # Join departement and region into communes
    df_join = df_iris[["commune_id", "departement_id", "region_id"]].groupby("commune_id").first().reset_index()
    df_communes = pd.merge(df_communes, df_join)

    # Reorder things
    df_iris = df_iris[["region_id", "departement_id", "commune_id", "iris_id", "geometry", "population"]]
    df_communes = df_communes[["region_id", "departement_id", "commune_id", "iris_id", "geometry", "population"]]

    # Count IRIS in communes
    commune_ids_without_iris = df_iris[df_iris["iris_id"].str.endswith("0000")]["commune_id"]
    df_communes["has_iris"] = ~df_communes["commune_id"].isin(commune_ids_without_iris)
    df_communes["zone_level"] = "commune"

    # Remove artificial IRIS
    df_iris = df_iris[~df_iris["iris_id"].str.endswith("0000")]
    df_iris["has_iris"] = True
    df_iris["zone_level"] = "iris"

    # Construct zones
    df_zones = pd.concat([df_communes, df_iris])
    df_zones["zone_level"] = df_zones["zone_level"].astype("category")
    df_zones["zone_id"] = np.arange(len(df_zones))

    # Impute AU
    df_au = pd.DataFrame(context.stage("data.spatial.au"), copy = True)
    df_au.loc[:, "matching_commune_id"] = df_au["commune_id"]

    df_zones.loc[:, "matching_commune_id"] = df_zones["commune_id"]
    df_zones.loc[df_zones["commune_id"].str.startswith("75"), "matching_commune_id"] = "75056"

    df_zones = pd.merge(df_zones, df_au[["matching_commune_id", "au"]], on = "matching_commune_id", how = "left")

    # Remove matching information again
    del df_zones["matching_commune_id"]

    # Fix unknown zones by distance
    f_unknown = df_zones["au"].isna()
    df_known = df_zones[~f_unknown]

    coordinates = np.vstack([df_known["geometry"].centroid.x, df_known["geometry"].centroid.y]).T
    kd_tree = KDTree(coordinates)

    df_unknown = df_zones[f_unknown]
    coordinates = np.vstack([df_unknown["geometry"].centroid.x, df_unknown["geometry"].centroid.y]).T
    indices = kd_tree.query(coordinates)[1].flatten()

    df_zones.loc[f_unknown, "au"] = df_known.iloc[indices]["au"].values

    # Check consistency
    assert(not df_zones["au"].isna().any())

    # Data types
    df_zones["region_id"] = df_zones["region_id"].astype(np.int)
    df_zones["departement_id"] = df_zones["departement_id"].astype(np.int)
    df_zones["commune_id"] = df_zones["commune_id"].astype(np.int)
    df_zones["iris_id"] = df_zones["iris_id"].astype(np.int)

    return df_zones[[
        "zone_id", "zone_level",
        "region_id", "departement_id", "commune_id", "iris_id", "geometry",
        "population", "au", "has_iris"
    ]]
