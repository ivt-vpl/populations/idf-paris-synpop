# Income in for the Paris scenario

The following document describes how income is handled in the Paris scenario.

## Available data

For Paris, income distributions are known from two data sources:

- The Enquête Globale Transport (the Household Travel Survey for Île-de-France)
- FILOSOFI (Income tax data)

In FILOSOFI, the net (*disponible*) and gross (*declaré*) annual income distributions for all *communes* are given in deciles. The income is the "income per consumption unit" in the household (*unité de consommation*). This means that each person in the household has a certain weight assigned:

- `1.0` for the *first* person over or equal 14 years old
- `0.5` for each *additional* person over or equal 14 years old
- `0.3` for each person under 14 years old

In the EGT, we have the sociodemographics of all household members. This means
we can calculate how many people have a certain age. The household income is given
as a categorical variable with the following upper bounds:

```
[800, 1200, 1600, 2000, 2400, 3000, 3500, 4500, 5500, np.inf]
```

From this information it is possible to reconstruct the *household income per
consumption unit* from the EGT. Currently, it is only used in the postprocessing
analysis. However, the distribution of that values, multiplied by 12, matches well
the distribution of the *net* income in FILOSOFI. Hence, we can construct a *net
annual income per consumption unit* from EGT data. In turn, what is there initially
in EGT is the *monthly total net household income*.

## Imputation

The imputation of income in the current state of the pipeline is rather simple.
For each commune, the income distribution from FILOSOFI is obtained. Since it
represents annual household income, the quantiles are divided by 12 to arrive
at monthly values. However, this does not consistute a full distribution yet, since
the largest number known to us from FILOSOFI is the 90\% quantile. In order to be
able to sample from the distribution, especially from the last decile, a maximum
value must be defined. At the moment, this number is fixed to an annual income of
*one million*. Note that this is a calibration parameter that is able to stretch
or squeeze the full distribution. Looking at the comparison between EGT (which
gives us individual data) and the quantiles of FILOSOFI shows that the proposed
maximum value leads to a good fit of the income distribution.

Then, for each *commune* all sampled households are obtained and an income value
is sampled for them. Remember, this is a *monthly net income per consumption
unit*. Note that no sociodemographics are involved here, though theoretically
we could come up with a more intelligent scheme (also because income distributions
for certain household categories are given in FILOSOFI). Also note that the matching
of activity chains happens *after* this step, i.e. a household that has obtained high
income here will later obtain activity chains from high income households.

## Output

The income is written out in the *households* file of MATSim. The value stated
there is the *monthly net income per consumption unit* that has been sampled
for each household as described above.

## Future work

There are some steps that can be improved:
- Make the sampling of incomes more intelligent (i.e. not only dependent on the
location via *commune*, but also depending on household type, e.g. single, couple, ...)
- Write out the consumption units of one household as a household attribute
- Write out the total household income (the currently given income value * consumption units)
