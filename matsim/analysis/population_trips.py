import gzip
from tqdm import tqdm
import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree
import numpy.linalg as la
import os

def configure(context, require):
    require.stage("matsim.run")
    require.stage("utils.java")
    require.stage("matsim.java.baseline")

def execute(context):
    java = context.stage("utils.java")

    population_path = "%s/ile_de_france_population.xml.gz" % context.external_cache_path("matsim.run")
    network_path = "%s/ile_de_france_network.xml.gz" % context.external_cache_path("matsim.run")
    output_path = "%s/trips.csv" % context.cache_path

    java(
        context.stage("matsim.java.baseline"), "ch.ethz.matsim.baseline_scenario.analysis.trips.run.ConvertTripsFromPopulation", [
            network_path, population_path, output_path
        ], cwd = context.cache_path)

    assert(os.path.exists(output_path))
    return pd.read_csv(output_path, sep = ";", encoding = "latin1")
