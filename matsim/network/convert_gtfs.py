import subprocess as sp
import os.path

def configure(context, require):
    require.stage("matsim.java.pt2matsim")
    require.stage("utils.java")
    require.config("raw_data_path")

def execute(context):
    jar = context.stage("matsim.java.pt2matsim")
    java = context.stage("utils.java")

    # Create MATSim schedule

    java(jar, "org.matsim.pt2matsim.run.Gtfs2TransitSchedule", [
        "%s/gtfs" % context.config["raw_data_path"],
        "20181106", "EPSG:2154",
        "%s/transit_schedule.xml.gz" % context.cache_path,
        "%s/transit_vehicles.xml.gz" % context.cache_path
    ], cwd = context.cache_path)

    assert(os.path.exists("%s/transit_schedule.xml.gz" % context.cache_path))
    assert(os.path.exists("%s/transit_vehicles.xml.gz" % context.cache_path))

    return {
        "schedule" : "%s/transit_schedule.xml.gz" % context.cache_path,
        "vehicles" : "%s/transit_vehicles.xml.gz" % context.cache_path
    }
