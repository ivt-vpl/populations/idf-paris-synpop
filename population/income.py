import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.neighbors import KDTree
import data.filo
import matplotlib.pyplot as plt
import multiprocessing as mp
import data.constants as c

def configure(context, require):
    require.stage("data.filo")
    require.stage("population.upscaled")
    require.config("random_seed", 1234)

MAXIMUM_INCOME = 1e6 / 12
INCOME_STEPS = 100

def parallel_initialize(_df_households, _distributions, _hts_choice, _random_seed):
    global df_households, distributions, hts_choice, random_seed
    df_households = _df_households
    distributions = _distributions
    hts_choice = _hts_choice
    random_seed = _random_seed

def parallel_run(args):
    i, commune_ids = args
    global df_households, distributions, random_seed
    incomes = np.arange(0, MAXIMUM_INCOME + 1, INCOME_STEPS)
    income_dfs = []

    np.random.seed(random_seed)

    for commune_id in tqdm(commune_ids, desc = "Imputing income", position = i):
        commune_incomes = []
        df_commune = pd.DataFrame(df_households[df_households["commune_id"] == commune_id], copy = True)

        df = distributions["total"]
        df = df[df["commune_id"] == commune_id]
        quantiles = list(df[["q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9"]].values.flatten() / 12.0)
        quantiles = np.array([0] + list(quantiles) + [quantiles[-1] * 100.0])
        ranges = quantiles[1:] - quantiles[:-1]
        pdf = 0.1 / ranges
        weights = pdf[[int(np.sum(i > quantiles[1:-1])) for i in incomes]]
        weights /= np.sum(weights)

        imputed_incomes = np.repeat(incomes, np.random.multinomial(len(df_commune), weights))
        np.random.shuffle(imputed_incomes)

        df_commune["income"] = imputed_incomes + np.random.random(size = (len(df_commune),)) * INCOME_STEPS
        income_dfs.append(df_commune)

    df_income = pd.concat(income_dfs)
    boundaries = c.ENTD_INCOME_BOUNDARIES if hts_choice.startswith("entd") else c.EGT_INCOME_BOUNDARIES
    df_income["income_class"] = np.digitize(df_income["income"], boundaries)

    df_income = df_income[["household_id", "income", "income_class"]]
    df_income.columns = ["household_id", "household_income", "household_income_class"]

    print() # Make progress bar consistent
    return df_income

def execute(context):
    df_households = pd.DataFrame(context.stage("population.upscaled")[[
        "household_size", "household_structure", "household_id", "commune_id"
    ]], copy = True).groupby("household_id").first().reset_index()

    df_filo = context.stage("data.filo")
    data.filo.impute_matching_clases(df_households)

    distributions = context.stage("data.filo")

    number_of_threads = context.config["threads"]
    pool = mp.Pool(
        processes = number_of_threads,
        initializer = parallel_initialize,
        initargs = (df_households, distributions, context.config["hts"], context.config["random_seed"])
    )

    commune_ids = list(set(np.unique(df_households["commune_id"])))

    chunks = np.array_split(commune_ids, number_of_threads)
    df_income = pd.concat(pool.map(parallel_run, enumerate(chunks)))
    pool.close()

    # Unité de consommation

    df_units = pd.DataFrame(context.stage("population.upscaled")[[
        "household_id", "age", "household_size"
    ]], copy = True)

    df_units["under_14"] = df_units["age"] < 14
    df_units["over_14"] = df_units["age"] >= 14
    df_units = df_units[[
        "household_id", "under_14", "over_14"
    ]].groupby("household_id").sum().reset_index()

    df_units["household_cus"] = df_units["under_14"] * 0.3 + 1.0 + np.maximum(0, df_units["over_14"] - 1) * 0.5
    df_units = df_units[["household_id", "household_cus"]]

    df_income = pd.merge(df_income, df_units, how = "left", on = "household_id")
    df_income["total_household_income"] = df_income["household_income"] * df_income["household_cus"]

    return df_income
