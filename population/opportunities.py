import gzip
from tqdm import tqdm
import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree
import numpy.linalg as la
import data.spatial.utils

def configure(context, require):
    require.stage("data.bpe.cleaned")
    require.stage("data.spatial.zones")

def execute(context):
    df_zones = context.stage("data.spatial.zones")[["zone_id", "commune_id", "zone_level", "geometry"]]
    df_zones = df_zones[df_zones["zone_level"] == "commune"]#[["zone_id", "commune_id"]]

    # Load data
    df_opportunities = pd.DataFrame(context.stage("data.bpe.cleaned")[[
        "bpe_id", "x", "y", "activity_type", "commune_id"
    ]], copy = True)
    df_opportunities.columns = ["location_id", "x", "y", "activity_type", "commune_id"]

    # Add attributes
    df_opportunities["offers_work"] = True
    df_opportunities["offers_other"] = True

    df_opportunities["offers_leisure"] = df_opportunities["activity_type"] == "leisure"
    df_opportunities["offers_shop"] = df_opportunities["activity_type"] == "shop"
    df_opportunities["offers_education"] = df_opportunities["activity_type"] == "education"

    df_opportunities["imputed"] = False

    # Sample opportunities for zones which do not have any observations
    commune_ids = set(np.unique(df_zones["commune_id"]))
    missing_work_ids = commune_ids - set(np.unique(df_opportunities["commune_id"]))
    missing_education_ids = commune_ids - set(np.unique(df_opportunities[df_opportunities["activity_type"] == "education"]["commune_id"]))

    print("Found %d communes without a work opportunity. Putting one opportunity at the centroid." % len(missing_work_ids))
    print("Found %d communes without an education opportunity. Putting one opportunity at the centroid." % len(missing_education_ids))

    rows = []
    start_index = np.max(df_opportunities["location_id"].values) + 1
    for index, commune_id in enumerate(missing_work_ids | missing_education_ids):
        df_zone = df_zones[df_zones["commune_id"] == commune_id].iloc[0]
        rows.append((
            start_index + index,
            df_zone.geometry.centroid.x, df_zone.geometry.centroid.y, commune_id,
            commune_id in missing_work_ids, commune_id in missing_education_ids
        ))

    df_imputed = pd.DataFrame.from_records(rows, columns = [
        "location_id", "x", "y", "commune_id", "offers_work", "offers_education"
    ])
    df_imputed["imputed"] = True
    df_imputed["offers_shop"] = False
    df_imputed["offers_leisure"] = False
    df_imputed["offers_other"] = False

    df_opportunities = pd.concat([
        df_opportunities[["location_id", "x", "y", "commune_id", "imputed", "offers_work", "offers_education", "offers_leisure", "offers_shop", "offers_other"]],
        df_imputed[["location_id", "x", "y", "commune_id", "imputed", "offers_work", "offers_education", "offers_leisure", "offers_shop", "offers_other"]]
    ])

    # Impute zone id
    df_opportunities = pd.merge(df_opportunities, df_zones, on = "commune_id")

    return df_opportunities
